﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Loading))]
public class LoadingEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Loading loading = (Loading)target;
        if (GUILayout.Button("Start Position"))
        {
            loading.SetLoadingStartPosition();
        }
        else if (GUILayout.Button("Middle Position"))
        {
            loading.SetLoadingPosition();
        }
        else if (GUILayout.Button("End Position"))
        {
            loading.SetLoadingEndPosition();
        }
    }
}
