﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(Badge))]
public class BadgeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Badge badge = (Badge)target;
        if (GUILayout.Button("Level Up"))
        {
            badge.Level++;
            badge.SetLevel(badge.Level);
        }
        else if (GUILayout.Button("Level Reset"))
        {
            badge.Level = 0;
            badge.SetLevel(badge.Level);
        }
    }
}
