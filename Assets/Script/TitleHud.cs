﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class TitleHud : MonoBehaviour {
    public Text LblCoin;
    public GameObject CoinPrefab;
    int _coinCount = 0;
    bool _isWaitingAnimation = false;
	// Use this for initialization
	void Start () {
        GameManager.Instance.TitleHud = this;
    }
	
	// Update is called once per frame
	void Update () {
        if (!_isWaitingAnimation && _coinCount != GameManager.Instance.GetCoin())
        {
            _coinCount = GameManager.Instance.GetCoin();
            UpdateCoinCount();
        }
	}
    public void UpdateCoinCount()
    {
        LblCoin.text = _coinCount.ToString();
    }
    public void AddCoin(int count, Vector3 pos)
    {
        _isWaitingAnimation = true;
        GameManager.Instance.AddCoin(count);
        float radius = 120;
        float moveToCoinTime = 0.5f;
        float initMoveTime = 0.3f;
        float delay = initMoveTime;
        float delayInterval = 0.05f;
        int coinUnit = 3;
        int totalCoin = 0;
        while (totalCoin + coinUnit < count)
        {
            GameObject obj = Instantiate(CoinPrefab);
            obj.transform.SetParent(transform);
            obj.transform.position = pos;
            Vector3 initPos = new Vector3(pos.x + Random.Range(-radius, radius), pos.y + Random.Range(-radius, radius));
            Transform imgCoin = transform.Find("lblCoin").Find("Image");
            obj.transform.DOMove(initPos, initMoveTime).SetEase(Ease.OutCirc);
            obj.transform.DOMove(imgCoin.position, moveToCoinTime).SetEase(Ease.OutCirc).SetDelay(delay).OnComplete(()=> {
                _coinCount += coinUnit;
                UpdateCoinCount();
                LblCoin.transform.DOKill();
                LblCoin.transform.localScale = new Vector3(0.9f, 0.9f);
                LblCoin.transform.DOScale(1, 0.3f).SetEase(Ease.OutElastic);
                Destroy(obj);
            });

            delay += delayInterval;
            totalCoin += coinUnit;
        }
        // play coin sound

        // if adding coin is less than coinUnit
        if (totalCoin == 0)
        {
            delay = 0;
            moveToCoinTime = 0.05f;
            initMoveTime = 0;
        }
        Invoke("SetAnimationFlagToFalse", delay + moveToCoinTime);
    }

    public void SetAnimationFlagToFalse()
    {
        _isWaitingAnimation = false;
    }
}
