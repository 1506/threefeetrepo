﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class MoveUpAndDestroy : MonoBehaviour {
    public float MoveY = 1.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartAction()
    {
        transform.DOMoveY(transform.position.y + MoveY, 1).SetEase(Ease.OutCirc).SetAutoKill(true);
        GetComponent<FadeOutAndDestroy>().StartFadeOut(0.2f, 1);
    }
}
