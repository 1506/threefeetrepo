﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class GameUI : MonoBehaviour {
    public GameObject Go;
	// Use this for initialization
	void Start () {
        
    }
    public void EnableGameStartPopup()
    {
        if (GameManager.Instance.GameData.IsTutorialOn)
        {
            transform.Find("TutorialPopup").gameObject.SetActive(true);
        }
        else
        {
            transform.Find("ItemPopup").gameObject.SetActive(true);
        }
    }
    public void OnCloseTutorialClick()
    {
        transform.Find("TutorialPopup").gameObject.SetActive(false);
    }
    public void OnNeverShowTutorialClick()
    {
        OnCloseTutorialClick();
        GameManager.Instance.GameData.IsTutorialOn = false;
    }
    public void OnAdvancedStartCoinClick()
    {
        int price = 100;
        if (GameManager.Instance.GetCoin() >= price)
        {
            GameManager.Instance.AddCoin(-price);
            GameManager.Instance.World.OnAutoClick();
            OnCloseItemPopupClick();
        }
        else
        {
            GameManager.Instance.ShowDisposableMessage(LanguageManager.Instance.GetText("not enough golds"));
        }
    }
    public void OnAdvancedStartVideoClick()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            OnCloseItemPopupClick();
        }
    }
    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                
                GameManager.Instance.World.OnAutoClick();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
    public void OnCloseItemPopupClick()
    {
        transform.Find("ItemPopup").gameObject.SetActive(false);
    }
    public void ShowGo()
    {
        GameObject obj = Instantiate(Go);
        obj.transform.SetParent(transform);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
