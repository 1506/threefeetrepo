﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class MultiPlayState : MonoBehaviour {
    List<MultiPlayerInfo> _playerList;
    public float StartX = 0;
    public float EndX = 100;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init()
    {
        gameObject.SetActive(true);
        _playerList = MultiplayerManager.Instance.PlayerList;
        for (int i = 0; i < _playerList.Count; i++)
        {
            transform.Find(string.Format("player{0}", i)).DOMoveX(StartX, 0.01f);
        }
    }
    public void MovePlayer(MultiPlayerInfo player, int score)
    {
        int index = _playerList.IndexOf(player);
        float targetX = (EndX - StartX) * score / GameManager.Instance.World.MultiplayGoalScore;
        transform.Find(string.Format("player{0}", index)).DOMoveX(targetX, 0.01f);
    }
}
