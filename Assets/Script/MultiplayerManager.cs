﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class MultiplayerManager : MonoBehaviour {
    private static MultiplayerManager _instance;
    public static MultiplayerManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(MultiplayerManager)) as MultiplayerManager;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "MultiplayerManager Instance" + UnityEngine.Random.Range(0, 1000);
                    _instance = container.AddComponent(typeof(MultiplayerManager)) as MultiplayerManager;
                    DontDestroyOnLoad(container);
                }
            }

            return _instance;
        }
    }

    public List<MultiPlayerInfo> PlayerList
    {
        get
        {
            return _playerList;
        }

        set
        {
            _playerList = value;
        }
    }

    string _gameName = "SM";
    int _maxUserCount = 4;
    int _roomNumber = -1;
    int _oldScore = 0;
    Client client = null;
    List<MultiPlayerInfo> _playerList = new List<MultiPlayerInfo>();
    MultiPlayerInfo _player;
    bool _isWaitingForPlayer = false;
    bool _defaultWaitTimeIsUp = false;
    public GameObject SearchPopup;
    bool _isRoomClosed = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_isWaitingForPlayer)
        {
            if (_defaultWaitTimeIsUp)
            {
                if (PlayerList.Count > 1)
                {
                    StartGame();
                }
            }else
            {
                if (PlayerList.Count >= 4)
                {
                    StartGame();
                }
            }
        }
    }
    public void StartGame()
    {
        _isWaitingForPlayer = false;
        _defaultWaitTimeIsUp = false;
        client.Send(ClientNetworkMessages.CloseRoom);
    }
    
    public void Connect()
    {
        Disconnect();
        client = new Client();
        client.StartConnect();
        client.OnSend += client_OnSend;
        client.OnReceive += client_OnReceive;
        client.OnConnected += Client_OnConnected;
        client.OnRoomIsFull += client_OnRoomIsFull;
        client.OnRoomInformation += client_OnRoomInformation;
        client.OnPlayerAdded += client_OnPlayerAdded;
        client.OnPlayerState += client_OnPlayerState;
        client.OnPlayerGone += client_OnPlayerGone;
        client.OnRoomClosed += Client_OnRoomClosed;
        _isRoomClosed = false;
    }
    public void Disconnect()
    {
        if (client != null)
        {
            client.Disconnect();
        }
    }
    private void Client_OnRoomClosed()
    {
        SearchPopup.GetComponent<MultiplaySearch>().StartGame();
        _isRoomClosed = true;
    }

    private void Client_OnConnected()
    {
        IWannaPlay(_gameName);
        Debug.Log("Client connected to server");
    }

    public void OnDisconnected()
    {
        PlayerList.Remove(_player);
    }
    void client_OnPlayerGone(string e)
    {
        if (_isRoomClosed)
        {

        }else
        {
            int index = (int)((char)e[0]);
            MultiPlayerInfo playerToRemove = null;
            foreach (MultiPlayerInfo player in PlayerList)
            {
                if (player.Index == index)
                {
                    playerToRemove = player;
                    break;
                }
            }
            if (playerToRemove != null)
            {
                PlayerList.Remove(playerToRemove);
            }
        }
    }

    void client_OnPlayerState(string e)
    {
        string[] strs = e.Substring(1).Split(',');
        int index = (int)((char)e[0]);
        int score = int.Parse(strs[0]);
        foreach (MultiPlayerInfo player in PlayerList)
        {
            if (player.Index == index)
            {
                player.Score = score;
                break;
            }
        }
    }

    void client_OnPlayerAdded(string e)
    {
        int index = (int)((char)e[0]);
        MultiPlayerInfo player = new MultiPlayerInfo();
        player.Index = index;
        PlayerList.Add(player);
    }

    void client_OnRoomInformation(string e)
    {
        byte[] array = Encoding.UTF8.GetBytes(e);
        byte[] roomNumberArray = new byte[2];
        Buffer.BlockCopy(array, 0, roomNumberArray, 0, 2);
        short roomNumber = BitConverter.ToInt16(roomNumberArray, 0);

        _roomNumber = roomNumber;
        _player.Index = (int)array[2];
        PlayerList.Add(_player);
        _defaultWaitTimeIsUp = false;
        _isWaitingForPlayer = true;
        Debug.Log("Room information: " + roomNumber);
    }

    void client_OnRoomIsFull()
    {
        
    }
    void client_OnReceive(string e)
    {
        
    }
    void client_OnSend(string e)
    {

    }

    public void OnSend(object sender, EventArgs e)
    {

    }

    public void IDRequest()
    {
        client.Send(ClientNetworkMessages.IDRequest);
    }

    public void NickNameRequest()
    {
        client.Send(ClientNetworkMessages.NickNameRequest);
    }
    public void SendScore(int score)
    {
        string text = string.Format("{0},{1},{2},{3}", _gameName, _player.Name, score, _oldScore);
        _oldScore = score;
        client.Send(ClientNetworkMessages.SendScore, text);
    }

    public void RankRequest()
    {
        client.Send(ClientNetworkMessages.RankRequest);
    }

    public void GoodBye()
    {
        PlayerList.Clear();
        client.Send(ClientNetworkMessages.GoodBye);
        client.Disconnect();
    }

    public void OnDestroy()
    {
        if (client != null)
            client.Disconnect();
    }

    public void UpdateData()
    {
        client.Send(ClientNetworkMessages.DataUpdate);
    }

    public void GiveMeMyData()
    {
        client.Send(ClientNetworkMessages.GiveMeMyInfo);
    }

    public void RequestOldData()
    {
        client.Send(ClientNetworkMessages.DoIHaveOldInfo);
    }

    public void TimeRequest()
    {
        client.Send(ClientNetworkMessages.WhatTimeIsIt);
    }

    public void RankToday()
    {
        client.Send(ClientNetworkMessages.RankTodayRequest, _gameName);
    }



    public void IWannaPlay(string name)
    {
        if (client != null)
        {
            _player.Name = name;
            client.Send(ClientNetworkMessages.LetMeIn, string.Format("{0},{1},{2},{3}", _gameName, _maxUserCount, 7, _player.Name));
        }
    }
    public void HereIsMine()
    {
        if (client != null)
        {
            client.Send(ClientNetworkMessages.HereIsMyState, _player.GetState());
        }
    }
    public void CheckHim()
    {
        if (client != null)
        {
            client.Send(ClientNetworkMessages.CheckHim, PlayerList[1].Index.ToString());
        }
    }

    public void Iamhere()
    {
        if (client != null)
        {
            client.Send(ClientNetworkMessages.AmIAlive);
        }
    }

    public void DayCount()
    {
        client.Send(ClientNetworkMessages.DayCountRequest, _gameName);
    }

    public void RankYesterday()
    {
        client.Send(ClientNetworkMessages.RankYesterdayRequest, _gameName);
    }
}
