﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Badge : MonoBehaviour {
    public int Level = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SetLevel(int level)
    {
        transform.Find("medal").gameObject.SetActive(false);
        if (level % 3 == 0)
        {
            transform.Find("medal").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/bronzeMedal");
        }else if(level %3 == 1)
        {
            transform.Find("medal").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/silverMedal");
        }
        else if (level % 3 == 2)
        {
            transform.Find("medal").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/goldMedal");
        }
        transform.Find("medal").gameObject.SetActive(true);
        transform.Find("grayString").gameObject.SetActive(level < 3);
        transform.Find("redString").gameObject.SetActive(level >= 3);
        transform.Find("greenString").gameObject.SetActive(level >= 6);
        transform.Find("blueString").gameObject.SetActive(level >= 9);

        transform.Find("upperWing").gameObject.SetActive(level >= 12);
        if (level >= 18)
        {
            transform.Find("upperWing").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/goldUpperWing");
        }else if (level >= 15)
        {
            transform.Find("upperWing").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/silverUpperWing");
        }
        else if (level >= 12)
        {
            transform.Find("upperWing").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/bronzeUpperWing");
        }

        transform.Find("lowerWing").gameObject.SetActive(level >= 21);
        if (level >= 27)
        {
            transform.Find("lowerWing").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/goldLowerWing");
        }
        else if (level >= 24)
        {
            transform.Find("lowerWing").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/silverLowerWing");
        }
        else if (level >= 21)
        {
            transform.Find("lowerWing").GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/badge/bronzeLowerWing");
        }

        transform.Find("star").gameObject.SetActive(level >= 30);
    }
}
