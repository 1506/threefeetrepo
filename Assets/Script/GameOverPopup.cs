﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class GameOverPopup : MonoBehaviour {
    public int Score = 0;
    public int CoinCount = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartCounting(int coin, int score)
    {
        CoinCount = coin;
        Score = score;
        StartCoroutine(CountingScore());
        StartCoroutine(CountingCoin());
        GameManager.Instance.AddCoin(CoinCount);
    }

    void OnVideoDoneForDoubleCoin()
    {
        GameManager.Instance.AddCoin(CoinCount);
        StartCoroutine(DoublingCoin());
    }
    public void OnDoubleCoinClick()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }
    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                OnVideoDoneForDoubleCoin();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
    IEnumerator CountingScore()
    {
        int pendingScore = 0;
        while (true)
        {
            pendingScore += 3;
            GameManager.Instance.PlaySound(gameObject);
            if (Score < pendingScore)
            {
                Score = pendingScore;
                transform.Find("lblScore").GetComponent<Text>().text = pendingScore.ToString();
                break;
            }
            transform.Find("lblScore").GetComponent<Text>().text = pendingScore.ToString();

            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator CountingCoin()
    {
        int pendingCoin = 0;
        while (true)
        {
            pendingCoin++;
            if (CoinCount <= pendingCoin)
            {
                transform.Find("lblCoin").GetComponent<Text>().text = pendingCoin.ToString();
                break;
            }
            transform.Find("lblCoin").GetComponent<Text>().text = pendingCoin.ToString();
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator DoublingCoin()
    {
        int pendingCoin = CoinCount;
        while (true)
        {
            pendingCoin++;
            if (CoinCount*2 <= pendingCoin)
            {
                transform.Find("lblCoin").GetComponent<Text>().text = pendingCoin.ToString();
                break;
            }
            transform.Find("lblCoin").GetComponent<Text>().text = pendingCoin.ToString();
            yield return new WaitForSeconds(0.1f);
        }
    }
}
