﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum LocalizeeType
{
    Text,
    Image
}
public class Localizer : MonoBehaviour {
    public LocalizeeType ComponentType;
    public string LocalizingKey = string.Empty;
    
	// Use this for initialization
	void Start () {
        UpdateLanguage();
        LanguageManager.Instance.LanguageChanged += OnLanguageChanged;
	}

    private void OnLanguageChanged(object sender, System.EventArgs e)
    {
        UpdateLanguage();
    }

    public void UpdateLanguage()
    {
        if (ComponentType == LocalizeeType.Image)
        {
            Debug.Log(LocalizingKey);
            GetComponent<Image>().sprite = Resources.Load<Sprite>(LanguageManager.Instance.GetText(LocalizingKey));
        }
        else if (ComponentType == LocalizeeType.Text)
        {
            GetComponent<Text>().text = LanguageManager.Instance.GetText(LocalizingKey);
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
