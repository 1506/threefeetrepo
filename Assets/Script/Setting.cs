﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Setting : MonoBehaviour {
    public Button BtnMusic;
    public Button BtnSound;
    
	// Use this for initialization
	void Start ()
    {
        GameManager.Instance.CurrentPopup = gameObject;
        UpdateButtons();
    }
	
	// Update is called once per frame
	void Update () {
        
    }
    private void UpdateButtons()
    {
        BtnMusic.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(GameManager.Instance.GameData.IsMusicOn ? "ui/musicOn" : "ui/musicOff");
        BtnSound.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(GameManager.Instance.GameData.IsSoundOn ? "ui/soundOn" : "ui/soundOff");
        transform.Find("tgTutorial").GetComponent<Toggle>().isOn = GameManager.Instance.GameData.IsTutorialOn;
    }
    public void OnIsTutorialOnChanged(bool isOn)
    {
        GameManager.Instance.GameData.IsTutorialOn = isOn;
    }
    public void OnSoundClick()
    {
        GameManager.Instance.GameData.IsSoundOn = !GameManager.Instance.GameData.IsSoundOn;
        UpdateButtons();
    }
    public void OnMusicClick()
    {
        GameManager.Instance.GameData.IsMusicOn = !GameManager.Instance.GameData.IsMusicOn;
        if (GameManager.Instance.GameData.IsMusicOn)
        {
            if (GameManager.Instance.BGMAudioSource != null)
            {
                GameManager.Instance.BGMAudioSource.Play();
            }
        }else
        {
            if (GameManager.Instance.BGMAudioSource != null)
            {
                GameManager.Instance.BGMAudioSource.Pause();
            }
        }
        UpdateButtons();
    }
    public void OnRestoreClick()
    {

    }
    public void OnCloseClick()
    {
        Destroy(gameObject);
        GameManager.Instance.CurrentPopup = null;
    }
    public void OnLanguageButtonClick(int lang)
    {
        if (lang == 0)  // english
        {
            LanguageManager.Instance.SetCurrentLanguage(SystemLanguage.English);
        }
        else if(lang == 1) // korean
        {
            LanguageManager.Instance.SetCurrentLanguage(SystemLanguage.Korean);
        }
    }
}
