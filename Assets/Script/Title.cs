﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        gameObject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {

    }
    private void Awake()
    {
        
    }
    public void OnPlayClick()
    {
        gameObject.SetActive(false);
        GameManager.Instance.World.Restart();
    }
    public void OnShopClick()
    {
        //AchievementManager.Instance.AddCurrentProgress(AchievementType.VisitShop30Times);
        //GameManager.Instance.UnlockCharacter(1); // test now
        //GameManager.Instance.SaveGameData();
        //SceneManager.LoadScene("shopScene");
        GameManager.Instance.LoadSceneAdditive("shopScene");
    }
    public void OnLeaderboardClick()
    {
        PlayServiceManager.Instance.ShowLeaderboard(LeaderboardType.BestScore);
    }
    public void OnAchievementClick()
    {
        AchievementPopup popup = transform.Find("AchievementPopup").GetComponent<AchievementPopup>();
        popup.ShowAchievement();
    }
    public void OnStatisticsClick()
    {
        AchievementManager.Instance.AddCurrentProgress(AchievementType.CheckStatistics50Times);
        transform.Find("StatisticsPopup").gameObject.SetActive(true);
        transform.Find("StatisticsPopup").GetComponent<Statistics>().UpdateValues();
    }
    public void OnMultiplayClick()
    {
        transform.Find("MultiplaySearchPopup").gameObject.SetActive(true);
        transform.Find("MultiplaySearchPopup").GetComponent<MultiplaySearch>().Search();
    }
}
