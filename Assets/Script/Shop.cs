﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Shop : MonoBehaviour {
    public GameObject SelectedItem;
    public Text LblDebug;
    int _selectedIndex = 0;
    public Button BuyButton;
    public ScrollRect ScrollRect;
    public SmartScrollView SmartScroll;
	// Use this for initialization
	void Start ()
    {
        SmartScroll.ItemSelected += OnItemSelected;
        SmartScroll.SelectItem(GameManager.Instance.GameData.SelectedCharacter);
        InAppPurchase.Instance.Purchased += OnPurchased;
	}

    private void OnPurchased(ProductIDs id)
    {
        if(id == ProductIDs.RemoveAds)
        {
            LblDebug.text = "Remove Ads purchased";
        }
    }

    private void OnDestroy()
    {
        InAppPurchase.Instance.Purchased -= OnPurchased;
    }

    

    private void OnItemSelected(GameObject obj)
    {
        SelectedItem = obj;
        int index = SmartScroll.SelectedIndex;
        _selectedIndex = index;
        bool unlocked = GameManager.Instance.IsCharacterUnlocked(index);
        if (unlocked || index == 0)
        {
            BuyButton.GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/play");
            BuyButton.transform.Find("Text").gameObject.SetActive(false);
        }
        else
        {
            BuyButton.GetComponent<Image>().sprite = Resources.Load<Sprite>("ui/unchecked");
            BuyButton.transform.Find("Text").gameObject.SetActive(true);
            BuyButton.transform.Find("Text").GetComponent<Text>().text = "100원";
        }
    }

    private void Update()
    {

    }
    
	// Update is called once per frame
	void FixedUpdate () {
       
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackClick();
        }

    }

    public void OnItemClick(int item)
    {
        _selectedIndex = item;
        SmartScroll.SelectItem(SmartScroll.ItemList[item]);
        SmartScroll.SetToSelectedItem();
        UpdateButtons();
    }
    public void UpdateButtons()
    {

    }
    public void OnBuyClick()
    {
        bool unlocked = GameManager.Instance.IsCharacterUnlocked(_selectedIndex);
        if (unlocked)
        {
            GameManager.Instance.SelectCharacter(SmartScroll.SelectedIndex);
            OnBackClick();
        }else
        {
            InAppPurchase.Instance.BuyProductID(ProductIDs.RemoveAds);
        }
    }
    public void OnCharacterUnlocked()
    {
        AchievementManager.Instance.UnlockAchievement(AchievementType.NewMonster);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.FiveNewMonsters);
    }
    public void OnBackClick()
    {
        AchievementManager.Instance.SaveAchievementData();
        Debug.Log(string.Format("shop Selected Character: {0}", GameManager.Instance.GameData.SelectedCharacter));
        GameManager.Instance.SaveGameData();
        GameManager.Instance.UnloadScene("shopScene");
        //SceneManager.UnloadSceneAsync("shopScene");
    }
}
