﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
public enum LoadingType
{
    Loading,
    LoadingAdditive,
    Loaded,
    Unloading
}
public class Loading : MonoBehaviour {
    public string SceneName;
    public float LoadingAnimationTime = 0.7f;
    AsyncOperation _asyncOperation;
    public LoadingType Type;
    public Vector3 LoadingPosition;
    public Vector3 LoadingStartPosition;
    public Vector3 LoadingEndPosition;
    public Vector3 Position
    {
        get { return transform.position; }
    }
    public void SetLoadingPosition()
    {
        LoadingPosition = transform.position;
    }
    public void SetLoadingStartPosition()
    {
        LoadingStartPosition = transform.position;
    }
    public void SetLoadingEndPosition()
    {
        LoadingEndPosition = transform.position;
    }
    public void HideLoading()
    {
        transform.position = LoadingPosition;
        transform.DOMove(LoadingEndPosition, LoadingAnimationTime).SetEase(Ease.InCirc).OnComplete(() => { Destroy(gameObject); });
    }
    IEnumerator Start()
    {
        if (Type == LoadingType.Loaded)
        {
            transform.position = LoadingPosition;
            transform.DOMove(LoadingEndPosition, LoadingAnimationTime).SetEase(Ease.InCirc).OnComplete(() => { Destroy(gameObject); });
        }
        else if (Type == LoadingType.LoadingAdditive)
        {
            transform.position = LoadingStartPosition;
            transform.DOMove(LoadingPosition, LoadingAnimationTime).SetEase(Ease.OutCirc).OnComplete(() => { /*Destroy(gameObject);*/ });

            Invoke("TransitScene", LoadingAnimationTime + 1);
            Debug.Log("**" + SceneName + "/time: " + LoadingAnimationTime);
            _asyncOperation = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);
            _asyncOperation.allowSceneActivation = false;

            yield return _asyncOperation;
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(SceneName));
        }
        else if (Type == LoadingType.Loading)
        {
            transform.position = LoadingStartPosition;
            transform.DOMove(LoadingPosition, LoadingAnimationTime).SetEase(Ease.OutCirc).OnComplete(() => { /*Destroy(gameObject);*/ });

            Invoke("TransitScene", LoadingAnimationTime +1 );
            Debug.Log("**" + SceneName + "time: " + LoadingAnimationTime);
            _asyncOperation = SceneManager.LoadSceneAsync(SceneName);
            _asyncOperation.allowSceneActivation = false;

            yield return _asyncOperation;
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(SceneName));
        }
        else if (Type == LoadingType.Unloading)
        {
            transform.position = LoadingStartPosition;
            transform.DOMove(LoadingPosition, LoadingAnimationTime).SetEase(Ease.OutCirc).OnComplete(() => { /*Destroy(gameObject);*/ });

            Invoke("TransitScene", LoadingAnimationTime +1);

            //_asyncOperation = SceneManager.UnloadSceneAsync(SceneName);
            //_asyncOperation.allowSceneActivation = false;

            //yield return _asyncOperation;
            //GameManager.Instance.PullOutLastLoadingStack();
        }
    }
    // Update is called once per frame
    void Update () {
		
	}

    void TransitScene()
    {
        if (Type == LoadingType.Unloading)
        {
            SceneManager.UnloadSceneAsync(SceneName);
            GameManager.Instance.PullOutLastLoadingStack();
        }
        else
        {
            _asyncOperation.allowSceneActivation = true;
        }
    }
}
