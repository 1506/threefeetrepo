﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MultiplayReady : MonoBehaviour {
    public GameObject ReadyImage;
    public GameObject PlayerInfoPrefab;
    public float InitX = 400;
    public float EndX = 800;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        gameObject.SetActive(true);
        ReadyImage.transform.DOMoveX(InitX, 0.001f);
        ReadyImage.transform.DOMoveX(EndX, 1).SetEase(Ease.OutCirc).SetDelay(0.001f);
        foreach (MultiPlayerInfo info in MultiplayerManager.Instance.PlayerList)
        {
            Transform group = transform.Find("HorizontalGroup");
            GameObject infoTile = Instantiate(PlayerInfoPrefab);
            infoTile.transform.SetParent(group);
            infoTile.transform.Find("lblName").GetComponent<Text>().text = info.Name;

            infoTile.transform.Find("character").GetComponent<Image>().sprite = Resources.Load<Sprite>(string.Format("monsterMini_{0}", info.Character));

            infoTile.transform.Find("badge").GetComponent<Badge>().SetLevel(info.Level);

            infoTile.transform.Find("index").GetComponent<Text>().text = MultiplayerManager.Instance.PlayerList.IndexOf(info).ToString();
        }
        Invoke("LetsBegin", 2);
        ReadyImage.transform.DOMoveX(InitX, 0.001f).SetEase(Ease.OutCirc).SetDelay(3);
    }
    public void LetsBegin()
    {
        gameObject.SetActive(false);
        GameManager.Instance.World.StartMultiplayGame();
    }
}
