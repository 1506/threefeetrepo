﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public enum LegPosition
{
    Near = 0,
    Far,
    Further,
}
public class Leg : MonoBehaviour {
    public LegPosition LegPosition = LegPosition.Near;
    public static float LegBaseY = -3.5f;
    public float FurtherY = LegBaseY + Tile.TileGapY * 2;
    public float FarY = LegBaseY + Tile.TileGapY * 1;
    public float NearY = LegBaseY + Tile.TileGapY * 0;
    public bool SkipOne = false;
    public float StepTime = 0.1f;
    public bool IsSuper = false;
    bool IsMoving = false;
    // Use this for initialization
    void Start() {

    }


    // Update is called once per frame
    void Update() {

    }
    public void GoToStart()
    {
        IsMoving = true;
        LegPosition = LegPosition.Near;
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(string.Format("images/leg_{0}", GameManager.Instance.GameData.SelectedCharacter));
        transform.DOMoveY(NearY, StepTime).OnComplete(()=> { IsMoving = false; }) ;
    }
    public void StepFurther()
    {
        IsMoving = true;
        SkipOne = true;
        if (LegPosition == LegPosition.Near)
        {
            LegPosition = LegPosition.Further;
            transform.DOMoveY(FurtherY, StepTime).OnComplete(() => { IsMoving = false; });
        }
    }
    public void StepForward()
    {
        IsMoving = true;
        if (LegPosition == LegPosition.Near)
        {
            LegPosition = LegPosition.Far;
            transform.DOMoveY(FarY, StepTime).OnComplete(() => { IsMoving = false; });
        } else if (LegPosition == LegPosition.Far)
        {
            LegPosition = LegPosition.Further;
            transform.DOMoveY(FurtherY, StepTime).OnComplete(() => { IsMoving = false; });
        }
    }
    public void StepBack()
    {
        if (LegPosition == LegPosition.Further)
        {
            LegPosition = LegPosition.Far;
            this.gameObject.transform.DOMove(new Vector2(transform.position.x, FarY), StepTime);
        }
        else if (LegPosition == LegPosition.Far)
        {
            LegPosition = LegPosition.Near;
            this.gameObject.transform.DOMove(new Vector2(transform.position.x, NearY), StepTime);
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    //{

    //}
    //private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (GameManager.Instance.World == null)
        {
            return;
        }

        Tile tile = collision.gameObject.GetComponent<Tile>() as Tile;
        if (tile == null)
        {
            return;
        }
        if (SkipOne || IsMoving)
        {
            if (tile.TileType == TileType.TileBomb)
            {
                AchievementManager.Instance.UnlockAchievement(AchievementType.SkipTheBomb);
            }
            SkipOne = false;
            return;
        }
        
        if (tile != null)
        {
            if (tile.TileType == TileType.TileNormal)
            {
                GameManager.Instance.World.IsStepedOnNormal = true;
            }
            else if (tile.TileType == TileType.TileBomb)
            {
                if (!IsSuper)
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(string.Format("images/legGone_{0}", GameManager.Instance.GameData.SelectedCharacter));
                    GameManager.Instance.World.GameOverWithBomb();
                }
                tile.transform.Find("object").GetComponent<FadeOutAndDestroy>().StartFadeOut(0.1f, 0);
                tile.Caboom();
                tile.TileType = TileType.TileNormal;
            }
            else if(tile.TileType == TileType.TileCoin)
            {
                GameManager.Instance.World.AddCoin();
                tile.transform.Find("object").GetComponent<MoveUpAndDestroy>().StartAction();
                tile.TileType = TileType.TileNormal;
            }
        }else
        {

        }
    }
}
