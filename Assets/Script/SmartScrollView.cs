﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class SmartScrollView : MonoBehaviour
{
    //public float CenterPosX = 384;
    public GameObject SelectedItem;
    public List<GameObject> ItemList = new List<GameObject>();
    ScrollRect ScrollRect;
    public event OnEventDelegate<GameObject> ItemSelected;
    float _timeSpan;
    float _navigatingItemTime = 0.3f;
    public int SelectedIndex
    {
        get { return ItemList.IndexOf(SelectedItem); }
    }
	// Use this for initialization
	void Start () {
        //CenterPosX = (transform.GetComponent<RectTransform>().sizeDelta.x + transform.Find("Viewport").Find("Content").GetComponent<RectTransform>().sizeDelta.x)/2;
        ScrollRect = GetComponent<ScrollRect>();
	}
	
	// Update is called once per frame
	void Update () {
        if (_timeSpan > 0)
        {
            _timeSpan -= Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        if (_timeSpan > 0)
        {
            return;
        }
        bool touchEnded = false;
        if (Input.touchCount <= 0 && !Input.GetMouseButton(0))
        {
            touchEnded = true;
        }
        //Debug.Log(string.Format("Content X: {0}, targetX: {1}", ScrollRect.transform.Find("Viewport").Find("Content").transform.localPosition.x, -SelectedItem.transform.localPosition.x + CenterPosX));
        if (touchEnded)
        {
            float speed = Mathf.Abs(ScrollRect.velocity.x);
            //Debug.Log(string.Format("Speed: {0}", speed));
            if (speed < 20)
            {
                ScrollRect.velocity = new Vector2();
                SetToSelectedItem();
            }
        }
    }

    public void SetToSelectedItem()
    {
        float x = -SelectedItem.transform.localPosition.x + GetComponent<RectTransform>().sizeDelta.x/2;
        if (x != ScrollRect.transform.Find("Viewport").Find("Content").transform.localPosition.x)
        {
            _timeSpan = _navigatingItemTime*2;
            Transform contentTransform = ScrollRect.transform.Find("Viewport").Find("Content");
            DOTween.To(() => contentTransform.localPosition, value => contentTransform.localPosition = value, new Vector3(x, 0, 0), _navigatingItemTime);
            
            //LeanTween.value(ScrollRect.transform.Find("Viewport").Find("Content").gameObject, updateNewValue, 
            //    ScrollRect.transform.Find("Viewport").Find("Content").transform.localPosition, new Vector3(x, 0, 0), _navigatingItemTime);
        }
    }
    private void updateNewValue(Vector3 vec)
    {
        ScrollRect.transform.Find("Viewport").Find("Content").transform.localPosition = vec;
    }
    public void OnScrollChanged(Vector2 pos)
    {
        float x, theX, scale;
        float minAllowGap = 30;
        float maxAllowGap = 168;
        float normalScale = 1;
        float selectedScale = 2;
        foreach (GameObject obj in ItemList)
        {
            x = obj.transform.position.x;
            theX = Mathf.Abs(x - GetComponent<RectTransform>().sizeDelta.x / 2 + pos.x);

            scale = normalScale + (selectedScale - normalScale) * ((maxAllowGap - theX) / (maxAllowGap - minAllowGap));
            if (scale > (normalScale + selectedScale) / 2)
            {
                //obj.transform.localScale = new Vector3(selectedScale, selectedScale, 1);
                obj.transform.localScale = new Vector3(scale, scale, 1);
                if (_timeSpan > 0)
                {
                    
                }else
                {
                    if (SelectedItem != obj)
                    {
                        SelectItem(obj);
                    }
                }
            }
            else if (theX < maxAllowGap)
            {
                obj.transform.localScale = new Vector3(scale, scale, 1);
            }
            else
            {
                obj.transform.localScale = new Vector3(normalScale, normalScale, 1);
            }
        }
    }
    public void SelectItem(int index)
    {
        SelectItem(ItemList[index]);
    }
    public void SelectItem(GameObject obj)
    {
        SelectedItem = obj;
        if (ItemSelected != null)
        {
            ItemSelected(obj);
        }
    }
}
