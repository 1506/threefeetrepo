﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class StrEventArg : EventArgs
{
    public string StringValue = string.Empty;
    public StrEventArg() { }
    public StrEventArg(string value)
    {
        StringValue = value;
    }
}
public class GameObjectEventArg : EventArgs
{
    public GameObject TheGameObject;
    public GameObjectEventArg() { }
    public GameObjectEventArg(GameObject value)
    {
        TheGameObject = value;
    }
}
public delegate void OnEventDelegate();
public delegate void OnEventDelegate<T1>(T1 arg1);
public delegate void OnEventDelegate<T1, T2>(T1 arg1, T2 arg2);
public class GameManager : MonoBehaviour
{
    public static System.Random Random = new System.Random();
    public static System.Random DailyRandom = new System.Random();
    public GameObject LoadingPrefab;
    public GameObject CurrentPopup;
    public TitleHud TitleHud;
    public float Power =10;
    public HelloWorld World;
    public AudioSource BGMAudioSource;
    public GameData GameData;
    private string _gameDataFileName = "/msfsyis.sis";
    public event EventHandler CharacterChanged;
    private static GameManager _instance;
    public bool IsLoading;
    public GameObject DisposableMessage;
    List<Loading> _loadingStack = new List<Loading>();
    public static GameManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "GameManager Instance" + Random.Next(1000);
                    _instance = container.AddComponent(typeof(GameManager)) as GameManager;
                    _instance.LoadingPrefab = Resources.Load<GameObject>("Prefabs/loading");
                    DontDestroyOnLoad(_instance);
                    _instance.LoadGameData();
                }
            }

            return _instance;
        }
    }
    

    public Loading LoadingScene { get; internal set; }

    private void OnApplicationPause(bool pause)
    {
        SaveGameData();
        AchievementManager.Instance.SaveAchievementData();
    }
    public void ShowDisposableMessage(string text)
    {
        GameObject message = Instantiate(DisposableMessage);
        message.transform.Find("lblMessage").GetComponent<Text>().text = text;
        message.GetComponent<FadeOutAndDestroy>().StartFadeOut(1, 1);
        message.transform.DOMoveY(100, 1).From(true);
    }
    public void LoadSceneWithAnimaition(string sceneName)
    {
        Canvas canvas = GameObject.FindObjectOfType(typeof(Canvas)) as Canvas;
        GameObject loading = Instantiate(LoadingPrefab);
        loading.transform.SetParent(canvas.transform);
        loading.GetComponent<Loading>().Type = LoadingType.Loading;
        loading.GetComponent<Loading>().SceneName = sceneName;
        IsLoading = true;
    }
    public void LoadSceneAdditive(string sceneName)
    {
        Canvas canvas = GameObject.FindObjectOfType(typeof(Canvas)) as Canvas;
        GameObject loadingObj = Instantiate(LoadingPrefab);
        loadingObj.transform.SetParent(canvas.transform);
        Loading loading = loadingObj.GetComponent<Loading>();
        loading.Type = LoadingType.LoadingAdditive;
        loading.SceneName = sceneName;
        _loadingStack.Add(loading);
        IsLoading = true;
    }
    public void UnloadScene(string sceneName)
    {
        Canvas canvas = GetCanvasInCurrentScene();
        GameObject loading = Instantiate(LoadingPrefab);
        loading.transform.SetParent(canvas.transform);
        loading.GetComponent<Loading>().Type = LoadingType.Unloading;
        loading.GetComponent<Loading>().SceneName = sceneName;
    }
    public Canvas GetCanvasInCurrentScene()
    {
        Canvas canvas = null;
        foreach (GameObject obj in SceneManager.GetActiveScene().GetRootGameObjects())
        {
            canvas = obj.GetComponent<Canvas>();
            if (canvas != null)
            {
                break;
            }
        }
        return canvas;
    }
    public void PullOutLastLoadingStack()
    {
        Loading lastLoading = _loadingStack[_loadingStack.Count - 1];
        lastLoading.HideLoading();
        _loadingStack.Remove(lastLoading);
    }
    public int GetCoin()
    {
        return GameData.CoinCount;
    }
    public void AddCoin(int coin)
    {
        GameData.CoinCount += coin;
        SaveGameData();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (CurrentPopup != null)
            {
                Destroy(CurrentPopup);
            }
        }
    }
    public void SelectCharacter(int index)
    {
        GameData.SelectedCharacter = index;
        if (CharacterChanged != null)
        {
            CharacterChanged(this, new EventArgs());
        }
    }
    public void SaveGameData()
    {
        Debug.Log("SaveGameData data");
        if (GameData  != null)
        {
            string path = Application.persistentDataPath + _gameDataFileName;
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = File.Open(path, FileMode.Open);

            bf.Serialize(fs, GameData);
            fs.Close();
        }
    }
    #region GameData Manipulation
    public int GetContinuousDayCount()
    {
        int currentDay = GameData.ContinuousDaycount;
        DateTime now = DateTime.Now;
        if (string.IsNullOrEmpty(GameData.LastPlayDay))
        {
            GameData.ContinuousDaycount = 1;
        }
        else
        {
            DateTime lastDay = Convert.ToDateTime(GameData.LastPlayDay);
            DateTime nextDay = lastDay.Add(new TimeSpan(1, 0, 0, 0));
            if (nextDay.Day == now.Day)
            {
                GameData.ContinuousDaycount++;
            }
        }
        string date = string.Format("{0:00}/{1:00}/{2:0000}", now.Day, now.Month, now.Year);
        DateTime dt = Convert.ToDateTime(date);
        Debug.Log(string.Format("CONTINUOUS DAY: {0}", date));
        return GameData.ContinuousDaycount;
    }
    public void UnlockCharacter(int index)
    {
        if (!GameData.UnlockedCharacters.Contains(index))
        {
            GameData.UnlockedCharacters.Add(index);
        }
    }
    public bool IsCharacterUnlocked(int index)
    {
        if (!GameData.UnlockedCharacters.Contains(0))
        {
            GameData.UnlockedCharacters.Add(0);
        }
        return GameData.UnlockedCharacters.Contains(index);
    }
    #endregion
    public void LoadGameData()
    {
        Debug.Log("Load data");
        bool loadedNormaly = false;
        try
        {
            if (File.Exists(Application.persistentDataPath + _gameDataFileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = File.Open(Application.persistentDataPath + _gameDataFileName, FileMode.Open);
                GameData = (GameData)bf.Deserialize(fs);
                fs.Close();
                loadedNormaly = true;
            }
        }
        catch (Exception)
        {
            loadedNormaly = false;
        }
        if (!loadedNormaly)
        {
            GameData = new GameData();
            string path = Application.persistentDataPath + _gameDataFileName;
            if (!File.Exists(path))
            {
                File.Create(path);
            }
        }
    }
    void Awake()
    {
        //if (!_instance)
        //{
        //    _instance = this;
        //    DontDestroyOnLoad(gameObject);
        //}
    }
    public void PlaySound(GameObject gameObj)
    {
        if (GameData.IsSoundOn)
        {
            AudioSource source = gameObj.GetComponent<AudioSource>();
            if (source != null)
            {
                source.Play();
            }
        }
    }
    public void PlayBGM(GameObject gameObj)
    {
        AudioSource source = gameObj.GetComponent<AudioSource>();
        if (source != null)
        {
            BGMAudioSource = source;
            if (GameData.IsMusicOn)
            {
                source.Play();
            }
        }
    }
    public static float GetAngle(Vector3 pos1, Vector2 pos2)
    {
        float xGap = pos2.x - pos1.x;
        float yGap = pos2.y - pos1.y;
        return Mathf.Atan2(yGap, xGap) * 180 / Mathf.PI;
    }
    public static float GetAngle(Vector2 pos1, Vector2 pos2)
    {
        float xGap = pos2.x - pos1.x;
        float yGap = pos2.y - pos1.y;
        return Mathf.Atan2(yGap, xGap) * 180 / Mathf.PI;
    }

    public static Vector3 GetDistanceByAngleAndSpeed(double angle, float speed)
    {
        double theta = angle * Math.PI / 180;
        return new Vector3((float)Math.Cos(theta) * speed, (float)Math.Sin(theta) * speed, 0);
    }
    public float GetDistance(Transform t1, Transform t2)
    {
        return Mathf.Sqrt(Mathf.Pow(t1.position.x - t2.position.x, 2) +
                        Mathf.Pow(t1.position.y - t2.position.y, 2) +
                        Mathf.Pow(t1.position.z - t2.position.z, 2));
    }

    public void MoveUpAndDestroy(GameObject obj, float scale, float time, float fadeOutDelay = 2)
    {
        obj.transform.DOMove(new Vector3(obj.transform.position.x, obj.transform.position.y + 1, 1), time).SetEase(Ease.OutSine);
        ScaleAndFadeOut(obj, time, scale, time, fadeOutDelay);
    }
    public void FadeOutAndDestroy(GameObject obj, float time, float delay = 0)
    {
        FadeOutAndDestroy fadeOut = fadeOut = obj.AddComponent<FadeOutAndDestroy>();
        fadeOut.StartFadeOut(time, delay);
    }
    public void ScaleAndFadeOut(GameObject obj, float scaleTime, float scale, float fadeTime, float fadeOutDelay = 1)
    {
        FadeOutAndDestroy(obj, fadeTime, fadeOutDelay);
        obj.transform.DOScale(scale, scaleTime).SetEase(Ease.OutElastic);
    }
}

