﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
using DG.Tweening;

public class ContinuePopup : MonoBehaviour {
    public HelloWorld World;
    public Text LblCount;
	// Use this for initialization
	void Start () {
		
	}
	public void StartCounting()
    {
        StartCoroutine(Counting());
    }
    private IEnumerator Counting()
    {
        for(int i = 5; i >= -1; i--)
        {
            if (i < 0)
            {
                World.OnCloseContinue();
            }else
            {
                LblCount.text = i.ToString();
                LblCount.transform.localScale = new Vector3(3, 3, 1);
                LblCount.transform.DOScale(2, 1).SetAutoKill(true);
                yield return new WaitForSeconds(1);
            }
        }
    }
    public void OnContinued()
    {
        gameObject.SetActive(false);
    }
    public void OnContinueClick()
    {
        StopCoroutine(Counting());
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }
    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                World.OnContinued();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
