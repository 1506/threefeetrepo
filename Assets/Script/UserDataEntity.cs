﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class UserDataEntity
{
    public string ID { get; set; }
    public string NickName { get; set; }
    public int Score { get; set; }
    public string Time { get; set; }
    public string Data { get; set; }
    public int Exp { get; set; }
    public int Level { get; set; }
    public string UserData0 { get; set; }
    public string UserData1 { get; set; }
    public string UserData2 { get; set; }
    public string UserData3 { get; set; }

    public UserDataEntity(int id, string nick, int score, string time, string data)
    {
        NickName = nick;
        Score = score;
        Time = time;
        ID = id.ToString();
        this.Data = data;
        Exp = 0;
        Level = 0;
        UserData0 = string.Empty;
        UserData1 = string.Empty;
        UserData2 = string.Empty;
        UserData3 = string.Empty;
    }

    public UserDataEntity() { }

    public override string ToString()
    {
        return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}", ID, NickName, Score, Time, Data, Exp, Level, UserData0, UserData1, UserData2, UserData3);
    }

    public static UserDataEntity FromString(string str)
    {
        string[] texts = str.Split('|');
        UserDataEntity entity = new UserDataEntity();
        if (texts.Length > 0)
        {
            entity.ID = texts[0];
        }
        if (texts.Length > 1)
        {
            entity.NickName = texts[1];
        }
        if (texts.Length > 2)
        {
            entity.Score = int.Parse(texts[2]);
        }
        if (texts.Length > 3)
        {
            entity.Time = texts[3];
        }
        if (texts.Length > 4)
        {
            entity.Data = texts[4];
        }
        if (texts.Length > 5)
        {
            entity.Exp = int.Parse(texts[5]);
        }
        if (texts.Length > 6)
        {
            entity.Level = int.Parse(texts[6]);
        }
        if (texts.Length > 7)
        {
            entity.UserData0 = texts[7];
        }
        if (texts.Length > 8)
        {
            entity.UserData1 = texts[8];
        }
        if (texts.Length > 9)
        {
            entity.UserData2 = texts[9];
        }
        if (texts.Length > 10)
        {
            entity.UserData3 = texts[10];
        }
        return entity;
    }
}
