﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
public class AdmobManager : MonoBehaviour {
    InterstitialAd interstitial=null;

    private static AdmobManager _instance;
    public static AdmobManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(AdmobManager)) as AdmobManager;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    _instance = container.AddComponent(typeof(AdmobManager)) as AdmobManager;
                    DontDestroyOnLoad(container);
                    container.name = "AdmobManager";
                    Debug.Log("AdmobManager not found so create one");
                }
            }

            return _instance;
        }
    }
    // Use this for initialization
    void Start () {
        
	}
    public void Init()
    {
        RequestInterstitial();
    }
    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "INSERT_ANDROID_BANNER_AD_UNIT_ID_HERE";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        BannerView bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }
    private void RequestInterstitial()
    {
        if (interstitial != null && interstitial.IsLoaded())
        {
            GameManager.Instance.World.LblDebug.text = "Admob not loading";
            return;
        }
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-4597682064467102~4802953874";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-4597682064467102/7756420274";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
        GameManager.Instance.World.LblDebug.text = "Admob requested";
    }
    // Update is called once per frame
    void Update () {
		
	}
    public void ShowInterstitial()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
            GameManager.Instance.World.LblDebug.text = "Admob show";
        }
        else
        {
            GameManager.Instance.World.LblDebug.text = "Admob not loaded";
        }
    }
    private void OnDestroy()
    {
        if(interstitial != null)
            interstitial.Destroy();
    }
}
