﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Statistics : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.SetActive(false);
        UpdateValues();
    }
    private void Awake()
    {
        UpdateValues();
    }
    public void UpdateValues()
    {
        Debug.Log("Update statistics");
        Text lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblGamePlayValue").GetComponent<Text>();
        int totalGamePlay = AchievementManager.Instance.GetCurrentProgress(AchievementType.Play3Times);
        lbl.text = totalGamePlay.ToString();

        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblBestScoreValue").GetComponent<Text>();
        int bestScore = AchievementManager.Instance.GetCurrentProgress(AchievementType.TenMeters);
        lbl.text = bestScore.ToString();

        int totalDistance = AchievementManager.Instance.GetCurrentProgress(AchievementType.Total10000Meters);
        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblTotalDistanceValue").GetComponent<Text>();
        lbl.text = totalDistance.ToString();

        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblAverageDistanceValue").GetComponent<Text>();
        if (totalGamePlay == 0)
        {
            lbl.text = "0m";
        }
        else
        {
            lbl.text = string.Format("{0:.0}m", (totalDistance * 1.0f / totalGamePlay));
        }

        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lbltotalStepsValue").GetComponent<Text>();
        lbl.text = AchievementManager.Instance.GetCurrentProgress(AchievementType.Total10000Steps).ToString();

        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblVisitShopValue").GetComponent<Text>();
        lbl.text = AchievementManager.Instance.GetCurrentProgress(AchievementType.VisitShop30Times).ToString();

        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblStatisticsValue").GetComponent<Text>();
        lbl.text = AchievementManager.Instance.GetCurrentProgress(AchievementType.CheckStatistics50Times).ToString();

        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblContinuousAttandanceValue").GetComponent<Text>();
        lbl.text = AchievementManager.Instance.GetCurrentProgress(AchievementType.FiveDaysInARow).ToString();

        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblShareValue").GetComponent<Text>();
        lbl.text = AchievementManager.Instance.GetCurrentProgress(AchievementType.Share).ToString();

        lbl = transform.Find("sptBackground").Find("valueLayoutGroup").Find("lblCoinCollectedValue").GetComponent<Text>();
        lbl.text = AchievementManager.Instance.GetCurrentProgress(AchievementType.Collect10CoinsTotal).ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnCloseClick()
    {
        gameObject.SetActive(false);
    }
}
