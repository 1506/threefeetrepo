﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
//using System.Threading.Tasks;
using System.Threading;

public enum ServerNetworkMessages
{
    Connected,
    IDCreated,
    NickNameReceived,
    ScoreReceived,
    HereIsRankInfo,
    HereIsRankTodayInfo,
    HereIsRankYesterdayInfo,
    HereIsDayCount,
    HereIsYourRank,
    DataUpdated,
    HereIsYourInfo,
    IsItYourInfo,
    NowIs,
    ByeBye,
    HereIsRoomInformation,
    HereIsPlayerState,
    PlayerAdded,
    PlayerGone,
    AreYouThere,
    RoomIsFull,
    YouAreAlive,
    RoomClosed
}
public enum ClientNetworkMessages
{
    IDRequest,
    NickNameRequest,
    GoodBye,
    SendScore,
    RankRequest,
    RankTodayRequest,
    RankYesterdayRequest,
    DayCountRequest,
    DataUpdate,
    GiveMeMyInfo,
    DoIHaveOldInfo,
    WhatTimeIsIt,
    LetMeIn,
    HereIsMyState,
    CheckHim,
    IAmHere,
    AmIAlive,
    CloseRoom
}
class Client
{
    static Socket SocketTCP = null; // Socket 변수 선언
    string address = "169.254.80.80";
    int port = 51142; // 서버의 포트값을 갖는다. 반드시 서버에서 설정된 포트값과 같아야 접속이 된다.
    byte[] buffer = new byte[4096]; // 서버에서 받은 데이터의 버퍼. 서버에서 보내지는 데이터를 이 버퍼에 계속 누적시킨다. 버퍼가 초과할 만큼 데이터가 많이 보내질 가능성이 있으면 버퍼 사이즈를 늘려줘야 한다.
    int recvlen = 0; // 실제로 받은 데이터의 길이
    public event OnEventDelegate<string> OnSend = null;
    public event OnEventDelegate<string> OnReceive = null;
    public int AvatarType = 0;
    private string _nickName = string.Empty;
    private string _id = string.Empty;
    private int score = 1;
    Thread _packetThread = null;
    UserDataEntity _entity = new UserDataEntity();
    public event OnEventDelegate<string> OnPlayerAdded;
    public event OnEventDelegate<string> OnPlayerGone;
    public event OnEventDelegate<string> OnPlayerState;
    public event OnEventDelegate<string> OnRoomInformation;
    public event OnEventDelegate OnRoomIsFull;
    public event OnEventDelegate OnConnected;
    public event OnEventDelegate OnRoomClosed;
    Random rand = new Random();
    public Client()
    {

    }
    public Client(string addr, int prt)
    {
        address = addr;
        port = prt;
    }
    public void SetAvatar(int type)
    {
        AvatarType = type;
    }

    public void StartConnect() // Canvas의 Start 버튼이 눌렸을때 호출된다.
    {
        Disconnect(); // 기존에 접속된 상태일 수 있기 때문에 먼저 접속을 초기화한다.

        try
        {
            IPAddress serverIP = IPAddress.Parse(address); // 서버 아이피 어드레스 설정
            int serverPort = Convert.ToInt32(port); // 포트값을 변환
            SocketTCP = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // TCP 소켓을 생성한다.
            SocketTCP.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 5000); // 송신 제한시간을 10초로 제한한다.
            SocketTCP.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000); // 수신 제한시간을 10초로 제한한다.
            SocketTCP.Connect(new IPEndPoint(serverIP, serverPort)); // 서버로 접속을 시도한다.

            StartPacketProcThread();
        }
        catch (Exception e)
        {
            //Console.WriteLine(e.ToString());
        }
    }
    private void StopPacketProcThread()
    {
        if (_packetThread != null)
        {
            _packetThread.Abort();
            _packetThread = null;
        }
    }
    private void StartPacketProcThread()
    {
        StopPacketProcThread();
        _packetThread = new Thread(new ThreadStart(PacketProc));
        _packetThread.Start();
    }

    public bool Send(ClientNetworkMessages msg) // 서버로 문자열을 보내준다.
    {
        string text = string.Empty;
        if (msg == ClientNetworkMessages.GoodBye)
        {
            text = string.Format("{0}{1}", (int)msg, "bye~");
        }
        else if (msg == ClientNetworkMessages.IDRequest)
        {
            text = string.Format("{0}{1}", (int)msg, "IDRequest~");
        }
        else if (msg == ClientNetworkMessages.NickNameRequest)
        {
            string nick = string.Format("Nick_{0}", rand.Next(100));
            _entity.NickName = nick;
            text = string.Format("{0}{1}", (int)msg, _entity.ToString());
        }
        else if (msg == ClientNetworkMessages.SendScore)
        {
            _entity.Score = rand.Next(1000);
            text = string.Format("{0}{1}", (int)msg, _entity.ToString());
        }
        else if (msg == ClientNetworkMessages.RankRequest)
        {
            text = string.Format("{0}{1}", (int)msg, _entity.ToString());
        }
        else if (msg == ClientNetworkMessages.RankTodayRequest)
        {
            text = string.Format("{0}{1}", (int)msg, _entity.ToString());
        }
        else if (msg == ClientNetworkMessages.DataUpdate)
        {
            text = string.Format("{0}{1}", (int)msg, _entity.ToString());
        }
        else if (msg == ClientNetworkMessages.GiveMeMyInfo)
        {
            text = string.Format("{0}{1}", (int)msg, _entity.ID);
        }
        else if (msg == ClientNetworkMessages.DoIHaveOldInfo)
        {
            text = string.Format("{0}{1}", (int)msg, _entity.UserData0);
        }
        else if (msg == ClientNetworkMessages.WhatTimeIsIt)
        {
            text = string.Format("{0}{1}", (int)msg, "?");
        }
        else if (msg == ClientNetworkMessages.LetMeIn)
        {
            text = string.Format("{0}{1}", (int)msg, "I wanna play");
        }
        else if (msg == ClientNetworkMessages.CheckHim)
        {
            text = string.Format("{0}{1}", (int)msg, "check him");
        }
        else if (msg == ClientNetworkMessages.IAmHere)
        {
            text = string.Format("{0}{1}", (int)msg, "I am here");
        }
        try
        {
            if (SocketTCP != null && SocketTCP.Connected) // 서버에 연결된 상태인지 체크
            {
                byte[] buff = new byte[4096]; // 보낼 데이터의 임시 버퍼

                // 처음 2바이트를 문자열 길이를 나타내는 것으로 한다. 여기에 문자열 헤더를 추가해도 되고, Json으로 변환도 가능하다.
                byte command = (byte)(((int)msg & 0x000000ff));
                // 처음 2바이트를 문자열 길이를 나타내는 것으로 한다. 여기에 문자열 헤더를 추가해도 되고, Json으로 변환도 가능하다.
                Buffer.BlockCopy(ShortToByte(Encoding.UTF8.GetBytes(text).Length + 3), 0, buff, 0, 2);
                buff[2] = command;
                // 실제 보낼 문자열 데이터를 byte로 변환해서 버퍼에 넣는다.
                Buffer.BlockCopy(Encoding.UTF8.GetBytes(text), 0, buff, 3, Encoding.UTF8.GetBytes(text).Length);

                // 데이터 길이 2바이트 + 실제 문자열 데이터로 구성된 버퍼를 서버로 송신한다.
                int result = SocketTCP.Send(buff, Encoding.UTF8.GetBytes(text).Length + 3, 0);

                if (OnSend != null)
                {
                    OnSend(text);
                }
                return true;
            }
        }
        catch (System.Exception)
        {
            return false;
        }
        return false;
    }
    public void Send(ClientNetworkMessages msg, string text) // 서버로 문자열을 보내준다.
    {
        try
        {
            if (SocketTCP != null && SocketTCP.Connected) // 서버에 연결된 상태인지 체크
            {
                byte[] buff = new byte[4096]; // 보낼 데이터의 임시 버퍼

                // text = string.Format("{0}{1}", (int)msg, "I am here");
                byte command = (byte)(((int)msg & 0x000000ff));
                // 처음 2바이트를 문자열 길이를 나타내는 것으로 한다. 여기에 문자열 헤더를 추가해도 되고, Json으로 변환도 가능하다.
                Buffer.BlockCopy(ShortToByte(Encoding.UTF8.GetBytes(text).Length + 3), 0, buff, 0, 2);
                buff[2] = command;
                // 실제 보낼 문자열 데이터를 byte로 변환해서 버퍼에 넣는다.
                Buffer.BlockCopy(Encoding.UTF8.GetBytes(text), 0, buff, 3, Encoding.UTF8.GetBytes(text).Length);

                // 데이터 길이 2바이트 + 실제 문자열 데이터로 구성된 버퍼를 서버로 송신한다.
                int result = SocketTCP.Send(buff, Encoding.UTF8.GetBytes(text).Length + 3, 0);

                if (OnSend != null)
                {
                    OnSend(text);
                }
            }
        }
        catch (System.Exception)
        {

        }
    }
    public void Disconnect() // 서버 연결을 끊는다.
    {
        StopPacketProcThread();
        Send(ClientNetworkMessages.GoodBye);
        if (SocketTCP != null)
        {
            SocketTCP.Close();
        }
    }

    public void PacketProc() // 패킷 처리 코루틴 함수
    {
        while (true)
        {
            if (SocketTCP.Connected)
            {

            }

            if (SocketTCP.Available > 0) // 소켓에서 들어온 데이터가 있으면 SocketTCP.Available은 수신받은 데이터 길이를 나타낸다.
            {
                byte[] buff = new byte[4096]; // 데이터를 받기 위한 임시 버퍼
                int nread = SocketTCP.Receive(buff, SocketTCP.Available, 0); // 소켓이 수신한 데이터를 buff로 읽어온다.

                if (nread > 0) // Receive 함수는 실제로 데이터를 받은 길이를 리턴한다. 이 값이 0 이상이면 실제로 뭔가를 받아온 상태이다.
                {
                    Buffer.BlockCopy(buff, 0, buffer, recvlen, nread); // 방금 받아온 데이터를 buffer에 누적시킨다.
                    recvlen += nread; // 실제로 받아온 데이터 길이를 증감시킨다.

                    while (true)
                    {
                        int length = BitConverter.ToInt16(buffer, 0); // 서버에서 보내지는 데이터도 맨앞의 2바이트는 길이를 나타내므로 길이를 얻어온다.

                        if (length > 0 && recvlen >= length) // 서버에서 실제로 받아온 데이터의 길이 recvlen이 문자열 길이 + 2바이트 보다 크면 패킷 1개가 온전히 도착했음을 의미한다.
                        {
                            ParsePacket(length); // 패킷 1개가 도착했기 때문에 파싱해서 처리한다.
                            recvlen -= length; // 패킷 처리가 끝났기 때문에 1개의 길이를 감소시킨다.
                            Buffer.BlockCopy(buffer, length, buffer, 0, recvlen); // 감소된 데이터 길이 만큼 버퍼를 제거해준다.
                        }
                        else
                        {
                            break; // 버퍼에 패킷이 없는 상태면 루프에서 빠져나간다. while 문으로 처리한 이유는 서버에서 1개 이상의 패킷이 동시에 이어서 올수도 있기 때문이다.
                        }
                    }
                }
            }

            Thread.Sleep(100);
        }
    }

    public static byte[] ShortToByte(int val) // int 변수를 2바이트 byte 데이터로 변환시켜주는 함수
    {
        byte[] temp = new byte[2];
        temp[1] = (byte)((val & 0x0000ff00) >> 8);
        temp[0] = (byte)((val & 0x000000ff));
        return temp;
    }

    public void ParsePacket(int length) // 서버에서 수신된 패킷을 파싱해서 처리하는 함수
    {
        string text = Encoding.UTF8.GetString(buffer, 3, length - 3);
        byte commandByte = buffer[2];
        ServerNetworkMessages msg = (ServerNetworkMessages)commandByte;//int.Parse(text.Substring(0, 1));
                                                                       //text = text.Substring(1);
                                                                       //string[] texts = text.Split(':');

        if (msg == ServerNetworkMessages.Connected)
        {
            if (OnConnected != null)
            {
                OnConnected();
            }
        }
        else if (msg == ServerNetworkMessages.IDCreated)
        {
            _entity.ID = text;
        }
        else if (msg == ServerNetworkMessages.NickNameReceived)
        {
            _entity.NickName = text;
        }
        else if (msg == ServerNetworkMessages.ScoreReceived)
        {

        }
        else if (msg == ServerNetworkMessages.HereIsRankTodayInfo)
        {
            Console.WriteLine("Rank today");
            int count = text.Length / 14;
            for (int i = 0; i < count; i++)
            {
                string name = text.Substring(i * 14, 10);
                int score = BitConverter.ToInt32(Encoding.UTF8.GetBytes(text.Substring(i * 14 + 10, 4)), 0);
                int index = name.IndexOf((char)0);
                name = name.Remove(index, 10 - index);

                string info = string.Format("{0}.{1}={2}", i + 1, name, score);
                Console.WriteLine(info);
            }
        }
        else if (msg == ServerNetworkMessages.HereIsRankYesterdayInfo)
        {
            int count = text.Length / 14;
            Console.WriteLine("Rank yesterday");
            for (int i = 0; i < count; i++)
            {
                string name = text.Substring(i * 14, 10);
                int score = BitConverter.ToInt32(Encoding.UTF8.GetBytes(text.Substring(i * 14 + 10, 4)), 0);
                Console.WriteLine("{0}.{1} - {2}", i + 1, name, score);
            }
        }
        else if (msg == ServerNetworkMessages.HereIsRankInfo)
        {
            List<UserDataEntity> list = new List<UserDataEntity>();
            string[] texts = text.Split('/');
            UserDataEntity mine = UserDataEntity.FromString(texts[0]);
            int index = 1;
            while (index < texts.Length)
            {
                list.Add(UserDataEntity.FromString(texts[index]));
                index++;
            }
            foreach (UserDataEntity entity in list)
            {
                //Console.WriteLine(entity.ToString());
            }
        }
        else if (msg == ServerNetworkMessages.DataUpdated)
        {
            if (!string.IsNullOrEmpty(text) && text.Equals("success"))
            {
                //Console.WriteLine("Update Data Success");
            }
            else
            {
                //Console.WriteLine("Update Data Failed");
            }
        }
        else if (msg == ServerNetworkMessages.HereIsYourInfo)
        {
            if (text.Equals("fail"))
            {
                //Console.WriteLine("GiveMeMyInfo Failed");
            }
            else
            {
                _entity = UserDataEntity.FromString(text);
                //Console.WriteLine(_entity.ToString());
            }
        }
        else if (msg == ServerNetworkMessages.HereIsYourRank)
        {
            Console.WriteLine("My Rank: {0}", (int)text[0]);
        }
        else if (msg == ServerNetworkMessages.HereIsDayCount)
        {
            Console.WriteLine("Day: {0}", BitConverter.ToInt16(Encoding.UTF8.GetBytes(text), 0));
        }
        else if (msg == ServerNetworkMessages.IsItYourInfo)
        {
            if (text.Equals("fail"))
            {
                //Console.WriteLine("I don't have old data");
            }
            else
            {
                //Console.WriteLine(text.ToString());
            }
        }
        else if (msg == ServerNetworkMessages.NowIs)
        {
            //Console.WriteLine("Now: {0}", text);
        }
        else if (msg == ServerNetworkMessages.ByeBye)
        {
            //Console.WriteLine("I am out? OMG");
            Disconnect();
        }
        else if (msg == ServerNetworkMessages.HereIsRoomInformation)
        {
            if (OnRoomInformation != null)
            {
                OnRoomInformation(text);
            }
        }
        else if (msg == ServerNetworkMessages.HereIsPlayerState)
        {
            if (OnPlayerState != null)
            {
                OnPlayerState(text);
            }
        }
        else if (msg == ServerNetworkMessages.RoomIsFull)
        {
            if (OnRoomIsFull != null)
            {
                OnRoomIsFull();
            }
        }
        else if (msg == ServerNetworkMessages.AreYouThere)
        {
            Send(ClientNetworkMessages.IAmHere);
        }
        else if (msg == ServerNetworkMessages.AreYouThere)
        {
            short dayCount = BitConverter.ToInt16(Encoding.UTF8.GetBytes(text), 0);
            Console.WriteLine("Daycount: {0}", dayCount);
        }
        else if (msg == ServerNetworkMessages.PlayerAdded)
        {
            if (OnPlayerAdded != null)
            {
                OnPlayerAdded(text);
            }
        }
        else if (msg == ServerNetworkMessages.PlayerGone)
        {
            if (OnPlayerGone != null)
            {
                OnPlayerGone(text);
            }
        }
        else if (msg == ServerNetworkMessages.RoomClosed)
        {
            if (OnRoomClosed != null)
            {
                OnRoomClosed();
            }
        }

        if (OnReceive != null)
        {
            OnReceive(text);
        }
    }

    void OnDestroy() // 앱이 종료되는 순간 호출되는 함수
    {
        if (SocketTCP != null && SocketTCP.Connected) // 서버에 접속된 상태이면
        {
            Send(ClientNetworkMessages.GoodBye); // 서버로 DISCONNECT 패킷을 보내 접속을 끊어준다.
            Thread.Sleep(500); // 서버로 패킷이 온전히 도착할때까지 기다려준다.
            SocketTCP.Close(); // 소켓을 닫는다.
        }
        StopPacketProcThread();
    }
}
