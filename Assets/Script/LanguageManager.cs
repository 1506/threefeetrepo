﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
public enum AvailableLanauge
{
    English,
    Korean
}
public class LanguageManager : MonoBehaviour {
    public string FilePath = "Resources/smlanguageSheet.csv";
    private Dictionary<string, Dictionary<string, string>> LanguageDic = new Dictionary<string, Dictionary<string, string>>();
    private string[] _languageArray;
    public event EventHandler LanguageChanged;
    #region Singleton
    private static LanguageManager _instance;
    public static LanguageManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(LanguageManager)) as LanguageManager;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "LanguageManager Instance" + UnityEngine.Random.Range(0, 1000);
                    _instance = container.AddComponent(typeof(LanguageManager)) as LanguageManager;
                }
                _instance.LoadCSV();
            }

            return _instance;
        }
    }
    #endregion
    // Use this for initialization
    void Start () {
    }
    public void LoadCSV()
    {

        var reader = new StreamReader(string.Format("{0}/{1}", Application.dataPath, FilePath));
        List<string> listA = new List<string>();
        List<string> listB = new List<string>();
        int index = 0;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var values = line.Split(',');
            if (index == 0)
            {
                foreach (string str in values)
                {
                    if (!string.IsNullOrEmpty(str))
                    {
                        Dictionary<string, string> dic = new Dictionary<string, string>();
                        LanguageDic.Add(str, dic);
                    }
                }
                _languageArray = values;
            }
            else
            {
                for (int i = 1; i < _languageArray.Length; i++)
                {
                    LanguageDic[_languageArray[i]].Add(values[0], values[i]);
                }
            }
            index++;
        }
    }
	public string GetText(string key)
    {
        string language = string.Empty;
        if (GetCurrentLanguage() == SystemLanguage.Korean)
        {
            language = "korean";
        }
        else // english
        {
            language = "english";
        }
        
        return LanguageDic[language][key];
    }
    public void SetCurrentLanguage(SystemLanguage lang)
    {
        GameManager.Instance.GameData.SelectedLanguage = (int)lang;
        if (LanguageChanged != null)
        {
            LanguageChanged(this, new EventArgs());
        }
    }
    public SystemLanguage GetCurrentLanguage()
    {
        SystemLanguage currentLang;
        if (GameManager.Instance.GameData.SelectedLanguage < 0) {
            currentLang = Application.systemLanguage;
        }else
        {
            currentLang = (SystemLanguage)GameManager.Instance.GameData.SelectedLanguage;
        }
        if (currentLang == SystemLanguage.Korean)
        {
            return SystemLanguage.Korean;
        }else
        {
            return SystemLanguage.English;
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
