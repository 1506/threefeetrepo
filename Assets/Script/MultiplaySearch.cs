﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplaySearch : MonoBehaviour {
    public GameObject ReadyPopup;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Search()
    {
        MultiplayerManager.Instance.Connect();
        MultiplayerManager.Instance.SearchPopup = this.gameObject;
    }

    public void StartGame()
    {
        ReadyPopup.GetComponent<MultiplayReady>().StartGame();
        gameObject.SetActive(false);
    }
    public void CloseSearch()
    {
        MultiplayerManager.Instance.Disconnect();
        gameObject.SetActive(false);
    }
}
