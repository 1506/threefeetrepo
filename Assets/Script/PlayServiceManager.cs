﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if !UNITY_IPHONE
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
#endif

public enum LeaderboardType
{
    BestScore,
}
public class PlayServiceManager : MonoBehaviour {

    private static PlayServiceManager _instance;
    public static PlayServiceManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(PlayServiceManager)) as PlayServiceManager;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    _instance = container.AddComponent(typeof(PlayServiceManager)) as PlayServiceManager;
                    container.name = "PlayServiceManager";
                    DontDestroyOnLoad(container);
                    Debug.Log("PlayServiceManager not found so create one");
                }
            }

            return _instance;
        }
    }
#if UNITY_ANDROID
    static PlayGamesClientConfiguration _GPGConfig;
#endif
    // Use this for initialization
    void Start () {

#if UNITY_ANDROID
        _GPGConfig = new PlayGamesClientConfiguration.Builder()
            .EnableSavedGames()
            .Build();

        PlayGamesPlatform.InitializeInstance(_GPGConfig);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
#endif

        Social.localUser.Authenticate(
                (bool success) =>
                {
                    Debug.Log(" - Social:SingIn= " + success.ToString());
                }
            );
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SendScore(long score, LeaderboardType type)
    {


        Social.ReportScore(score, GetLeaderboardID(type),
                (bool success) =>
                {
                    Debug.Log(string.Format("Send Score success! {0}", score));
                }
            );
    }
    public void ShowLeaderboard(LeaderboardType type)
    {
#if UNITY_ANDROID
        PlayGamesPlatform.Instance.ShowLeaderboardUI(GetLeaderboardID(type));
#elif UNITY_IPHONE
        Social.ShowLeaderboardUI();
# endif
    }
    public string GetLeaderboardID(LeaderboardType type)
    {
        if (type == LeaderboardType.BestScore)
        {
#if UNITY_ANDROID
            return "CgkI2-XXXXXXXXXAIQAQ"; //구글개발자 콘솔에서 얻은 리더보드 값
#elif UNITY_IPHONE
        return "SOCIALTEST_HIGH_SCORE"; //아이튠즈 커넥트에서 얻은 리더보드 값
#endif
        }
        return string.Empty;
    }

    public void ShowAchievement()
    {
#if UNITY_ANDROID
        PlayGamesPlatform.Instance.ShowAchievementsUI();
#elif UNITY_IPHONE
        Social.ShowAchievementsUI();
#endif
    }
    public void IncrementAchievement(string id, int increments, int total)
    {
#if UNITY_ANDROID
        PlayGamesPlatform.Instance.IncrementAchievement(id, increments, (bool success) =>
        {
            //Debug.Log(string.Format("Send Achievement success! {0}:{1}", id, increments));
        });
#elif UNITY_IPHONE
        
        Social.ReportProgress(id, total, (bool success) =>
        {
            Debug.Log(string.Format("ReportProgress success! {0}:{1}", id, total));
        });
#endif
    }
    
    public void UpdateAchievement(string id, float percentage)
    { 
#if UNITY_ANDROID
        PlayGamesPlatform.Instance.ReportProgress(id, percentage, (bool success) =>
        {
            //Debug.Log(string.Format("UpdateAchievement success! {0}:{1}", id, percentage));
        });
#elif UNITY_IPHONE
        
        Social.ReportProgress(id, percentage, (bool success) =>
        {
            Debug.Log(string.Format("UpdateAchievement success! {0}:{1}", id, percentage));
        });
#endif
    }
}
