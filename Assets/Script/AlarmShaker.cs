﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AlarmShaker : MonoBehaviour {
    
    private Sequence _seq;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartAlarm()
    {
        float dur = 0.04f;
        int angle = 5;
        _seq = DOTween.Sequence();
        _seq.Append(transform.DORotate(new Vector3(0, 0, angle), dur));
        _seq.Append(transform.DORotate(new Vector3(0, 0, -angle), dur * 2));
        _seq.Append(transform.DORotate(new Vector3(0, 0, angle), dur * 2));
        _seq.Append(transform.DORotate(new Vector3(0, 0, -angle), dur * 2));
        _seq.Append(transform.DORotate(new Vector3(0, 0, angle), dur * 2));
        _seq.Append(transform.DORotate(new Vector3(0, 0, 0), dur));
        _seq.AppendInterval(0.7f);
        _seq.SetLoops(-1);
        _seq.Play();
    }

    public void StopAlarm()
    {
        _seq.Kill();
    }
}
