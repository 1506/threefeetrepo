﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class AchievementData
{
    [XmlIgnore]
    public List<int> MaxProgressList;
    public List<int> CurrentProgressList = new List<int>();
    public List<bool> RewardedList = new List<bool>();
    [XmlIgnore]
    public List<string> iOSKeyList = new List<string>();
    [XmlIgnore]
    public List<string> androidKeyList;
    public AchievementData()
    {
        
    }
    public void Init()
    {
        MaxProgressList = new List<int>(new int[] {
                                        1,  // first step
                                        1,  // gogogo
                                        1,  // skip the bomb
                                        1,  // Try again
                                        1,  // Continue
                                        3,  // Play 3 times
                                        1,  // Share
                                        1,  // start from 30m
                                        5,  // 5days in a row
                                        10, // 10m walk
                                        1,  // a new monster
                                        50, // 50m
                                        100,// total 100 steps
                                        100,// total 100m
                                        100,// 100m
                                        50, // check statistics 50 times
                                        40, // visit shop 30 times
                                        3,  // share 3 times
                                        1000,   // total 1000 steps
                                        1000,   // total 1000m
                                        5,  // 5 new monsters
                                        100,// total 100 games
                                        10000,  // total 10000 steps
                                        10000,  // total 10000m
                                        1,  // collect a coin
                                        10, // total 10 coins
                                        5,  // 5 coins
                                        50, // total 50 coins
                                        10, // 10 coins
                                        100,// total 100 coins 
                                        15, // 15 coins
                                        200,// total 200 coins
                                        500,// total 500 coins
                                        1000,   // total 1000 coins
                                        });

        androidKeyList = new List<string>(new string[] {
                                        "CgkIusrY-98TEAIQAA",
                                        "CgkIusrY-98TEAIQAQ",
                                        "CgkIusrY-98TEAIQAg",
                                        "CgkIusrY-98TEAIQAw",
                                        "CgkIusrY-98TEAIQBA",
                                        "CgkIusrY-98TEAIQBQ",
                                        "CgkIusrY-98TEAIQBg",
                                        "CgkIusrY-98TEAIQBw",
                                        "CgkIusrY-98TEAIQCA",
                                        "CgkIusrY-98TEAIQCQ",
                                        "CgkIusrY-98TEAIQCg",
                                        "CgkIusrY-98TEAIQCw",
                                        "CgkIusrY-98TEAIQDA",
                                        "CgkIusrY-98TEAIQDQ",
                                        "CgkIusrY-98TEAIQDg",
                                        "CgkIusrY-98TEAIQDw ",
                                        "CgkIusrY-98TEAIQEA",
                                        "CgkIusrY-98TEAIQEQ",
                                        "CgkIusrY-98TEAIQEg",
                                        "CgkIusrY-98TEAIQEw",
                                        "CgkIusrY-98TEAIQFA",
                                        "CgkIusrY-98TEAIQFQ",
                                        "CgkIusrY-98TEAIQFg",
                                        "CgkIusrY-98TEAIQFw",
                                        "CgkIusrY-98TEAIQGA",
                                        "CgkIusrY-98TEAIQGQ",
                                        "CgkIusrY-98TEAIQGg",
                                        "CgkIusrY-98TEAIQGw",
                                        "CgkIusrY-98TEAIQHA",
                                        "CgkIusrY-98TEAIQHQ",
                                        "CgkIusrY-98TEAIQHg",
                                        "CgkIusrY-98TEAIQHw",
                                        "CgkIusrY-98TEAIQIA",
                                        "CgkIusrY-98TEAIQIQ"
                                        });
    }
}
public enum AchievementType
{
    TheFirstStep=0,
    GoGoGo,
    SkipTheBomb,
    TryAgain,
    Continue,
    Play3Times,
    Share,
    StartFrom30,
    FiveDaysInARow,
    TenMeters,
    NewMonster,
    FiftyMeters,
    Total100Steps,
    Total100Meters,
    HundredMeters,
    CheckStatistics50Times,
    VisitShop30Times,
    Share3Times,
    Total1000Steps,
    Total1000Meters,
    FiveNewMonsters,
    Total100Games,
    Total10000Steps,
    Total10000Meters,
    Collect1Coin,
    Collect10CoinsTotal,
    Collect5CoinsInSinglePlay,
    Collect50CoinsTotal,
    Collect10CoinsInSinglePlay,
    Collect100CoinsTotal,
    Collect15CoinsInSinglePlay,
    Collect200CoinsTotal,
    Collect500CoinsTotal,
    Collect1000CoinsTotal
}
public class AchievementManager : MonoBehaviour
{
    public AchievementData TheData;
    private string _achievementDataFileName = "/achievement.dat";
    private static AchievementManager _instance;
    public static AchievementManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(AchievementManager)) as AchievementManager;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "Achievement Instance" + UnityEngine.Random.Range(0, 1000);
                    _instance = container.AddComponent(typeof(AchievementManager)) as AchievementManager;
                    _instance.LoadAchievementData();
                    DontDestroyOnLoad(container);
                    Debug.Log("Achievement manager not found so create one");
                }
            }

            return _instance;
        }
    }
    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

    }
    public int GetTotalAchievementCount()
    {
        return TheData.MaxProgressList.Count;
    }
    public void SaveAchievementData()
    {
        if (TheData != null)
        {
            string path = Application.persistentDataPath + _achievementDataFileName;
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = File.Open(path, FileMode.Open);

            bf.Serialize(fs, TheData);
            fs.Close();
        }
    }

    public void LoadAchievementData()
    {
        bool loadedNormaly = false;
        try
        {
            if (File.Exists(Application.persistentDataPath + _achievementDataFileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = File.Open(Application.persistentDataPath + _achievementDataFileName, FileMode.Open);
                TheData = (AchievementData)bf.Deserialize(fs);
                fs.Close();
                loadedNormaly = true;
            }
        }
        catch (Exception)
        {
            loadedNormaly = false;
        }
        if(!loadedNormaly)
        {
            TheData = new AchievementData();
            string path = Application.persistentDataPath + _achievementDataFileName;
            if (!File.Exists(path))
            {
                File.Create(path);
            }
        }
        TheData.Init();
    }

    public string GetAchievementKey(AchievementType achieve)
    {
        string key = string.Empty;
#if UNITY_ANDROID
        key = TheData.androidKeyList[(int)achieve];
#elif UNITY_IPHONE
        key = TheData.iOSKeyList[(int)achieve];
#else
        string adUnitId = "unexpected_platform";
#endif
        return key;
    }

    public void AddCurrentProgress(AchievementType achieve, int amount)
    {
        int index = (int)achieve;
        TheData.CurrentProgressList[index] += amount;
        string key = GetAchievementKey(achieve);
        PlayServiceManager.Instance.IncrementAchievement(key, amount, TheData.CurrentProgressList[index]);
    }

    public int GetCurrentProgress(AchievementType achieve)
    {
        int index = (int)achieve;
        CreateIfNotExist(index);
        return TheData.CurrentProgressList[index];
    }
    public int GetMaxProgress(AchievementType achieve)
    {
        int index = (int)achieve;
        CreateIfNotExist(index);
        return TheData.MaxProgressList[index];
    }
    public void CreateIfNotExist(int index)
    {
        while (TheData.CurrentProgressList.Count <= index)
        {
            TheData.CurrentProgressList.Add(0);
        }
        while(TheData.MaxProgressList.Count <= index)
        {
            TheData.MaxProgressList.Add(1);
        }
        while (TheData.RewardedList.Count <= index)
        {
            TheData.RewardedList.Add(false);
        }
        while (TheData.iOSKeyList.Count <= index)
        {
            TheData.iOSKeyList.Add(string.Empty);
        }
        while (TheData.androidKeyList.Count <= index)
        {
            TheData.androidKeyList.Add(string.Empty);
        }
    }
    public void Rewarded(int index)
    {
        TheData.RewardedList[index] = true;
    }
    public void UpdateCurrentProgress(AchievementType achieve, int amount)
    {
        int current = GetCurrentProgress(achieve);
        string key = GetAchievementKey(achieve);
        if (current < amount)
        {
            int index = (int)achieve;
            TheData.CurrentProgressList[index] = amount;

            PlayServiceManager.Instance.UpdateAchievement(key, amount * 100.0f / TheData.CurrentProgressList[index]);
        }
    }
    public void UnlockAchievement(AchievementType achieve)
    {
        string key = GetAchievementKey(achieve);
        int index = (int)achieve;
        TheData.CurrentProgressList[index] = 1;
        PlayServiceManager.Instance.UpdateAchievement(key, 100);
    }

    public void AddCurrentProgress(AchievementType achieve)
    {
        AddCurrentProgress(achieve, 1);
    }
}
