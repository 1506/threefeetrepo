﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutAndDestroy : MonoBehaviour {
    float _timeLeft;
    float _maxTime;
    float _delayTime;
    
    bool _isStarted = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_isStarted)
        {
            if (_delayTime > 0)
            {
                _delayTime -= Time.deltaTime;
                return;
            }
            _timeLeft -= Time.deltaTime;
            if (_timeLeft < 0)
            {
                Destroy(gameObject);
                return;
            }
            SetAlpha(this.gameObject, _timeLeft / _maxTime);
        }
	}

    void SetAlpha(GameObject obj, float alpha)
    {
        SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>() as SpriteRenderer;
        if (renderer != null)
        {
            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, alpha);
        }
        foreach (Transform child in obj.transform)
            SetAlpha(child.gameObject, alpha);
    }

    public void StartFadeOut(float time, float delay)
    {
        _delayTime = delay;
        _timeLeft = time;
        _maxTime = time;
        _isStarted = true;
    }
}
