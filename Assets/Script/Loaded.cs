﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loaded : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (GameManager.Instance.IsLoading)
        {
            Canvas canvas = GameManager.Instance.GetCanvasInCurrentScene();
            GameObject loading = Instantiate(GameManager.Instance.LoadingPrefab);
            loading.transform.SetParent(canvas.transform);
            GameManager.Instance.IsLoading = false;
            loading.GetComponent<Loading>().Type = LoadingType.Loaded;
        }
    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
