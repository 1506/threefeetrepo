﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum AchievementItemStatus
{
    NotAchieved,
    AchievedNotRewarded,
    AchievedRewarded
}
public class AchievementItem : MonoBehaviour, System.ICloneable
{
    public int CurrentProgress = 0;
    public int MaxProgress = 1;
    public string AndroidKey;
    public string iOSKey;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
    public void SetProgress(int current)
    {
        SetProgress(current, MaxProgress);
    }
    public void OnRewardClick()
    {
        TitleHud hud = GameManager.Instance.TitleHud;
        int coinCount = int.Parse(transform.Find("btnReward").Find("lblCoin").GetComponent<Text>().text);
        hud.AddCoin(coinCount, transform.Find("btnReward").transform.position);
        int index = GetIndex();
        AchievementManager.Instance.Rewarded(index);
        AchievementPopup popup = transform.parent.parent.parent.parent.GetComponent<AchievementPopup>();
        popup.UpdateItems();
    }
    public void SetProgress(int current, int max)
    {
        if (current > max)
        {
            current = max;
        }
        CurrentProgress = current;
        MaxProgress = max;
        transform.Find("progress").GetComponent<Slider>().value = current * 1.0f / max;
        transform.Find("progress").Find("lblProgress").GetComponent<Text>().text = string.Format("{0}/{1}", current, max);
        int index = GetIndex();
        transform.Find("btnReward").GetComponent<Button>().enabled = current >= max && !AchievementManager.Instance.TheData.RewardedList[index];
    }

    public int GetIndex()
    {
        AchievementPopup popup = transform.parent.parent.parent.parent.GetComponent<AchievementPopup>();
        return popup.AchievementItemList.IndexOf(this);
    }
    
    object ICloneable.Clone()
    {
        AchievementItem item = new AchievementItem();
        item.CurrentProgress = CurrentProgress;
        item.MaxProgress = MaxProgress;
        item.iOSKey = iOSKey;
        item.AndroidKey = AndroidKey;
        return item;
    }
}
