﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class MultiPlayerInfo
{
    private string _name;
    private int _level = 0;
    private int _index = 0;
    private int _score = 0;
    private int _character = 0;
    public int Index
    {
        get { return _index; }
        set { _index = value; }
    }
    public int Level
    {
        get { return _level; }
        set { _level = value; }
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public int Score
    {
        get
        {
            return _score;
        }

        set
        {
            _score = value;
        }
    }

    public int Character
    {
        get
        {
            return _character;
        }

        set
        {
            _character = value;
        }
    }

    public MultiPlayerInfo()
    {

    }
    public MultiPlayerInfo(string name, int level)
    {
        _name = name;
        _level = level;
        Random rand = new Random();
    }
    public string GetIntroduce()
    {
        return string.Format("{0}{1}{2}", _index, _name, _level);
    }
    public string GetState()
    {
        return string.Format("{0}", _score);
    }
    public void SetState(string state)
    {
        string[] strs = state.Split(',');
        _score = int.Parse(strs[0]);
        _character = int.Parse(strs[1]);
    }
}
