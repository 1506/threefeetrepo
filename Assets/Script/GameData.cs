﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

[Serializable]
public class GameData
{
    public bool IsSoundOn = true;
    public bool IsMusicOn = true;
    public int SelectedLanguage = -1;
    public int SelectedCharacter = 0;
    public int CoinCount = 0;
    public int GemCount = 0;
    public List<int> UnlockedCharacters = new List<int>();
    public int ContinuousDaycount = 0;
    public string LastPlayDay = string.Empty;
    public bool IsTutorialOn = true;
    public GameData()
    {
        Init();
    }
    public void Init()
    {
        
    }
}