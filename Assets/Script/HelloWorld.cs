﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;
using UnityEngine.Advertisements;
using DG.Tweening;
public class HelloWorld : MonoBehaviour {
    public bool IsGameOver = false;
    public GameObject PauseMenu;
    public ContinuePopup ContinuePopup;
    public Text LblDebug;
    private bool ContinueUsed = false;
    public GameObject GameOverMenu;
    List<Tile> _tileList = new List<Tile>();
    float StepTime = 0.1f;
    public GameObject NormalTilePrefab;
    public GameObject BombTile;
    public GameObject CoinTile;
    Vector2 Size = new Vector2(7.5f, 10);
    public Leg LeftLeg;
    public Leg MiddleLeg;
    public Leg RightLeg;
    public float YGap = 1.6f;
    public float XGap = 1.6f;
    public float OffsetY = -2.5f;
    public Button BtnLeft;
    public Button BtnMiddle;
    public Button BtnRight;
    public LegPosition LeftPosition;
    public LegPosition MiddlePosition;
    public LegPosition RightPosition;
    public Text LblScore;
    public bool IsStepedOnBomb;
    public bool IsStepedOnNormal;
    public int Score = 0;
    int tileCountShouldBe = 0;
    float gameOverTimeForTorn = 0.4f;
    float gameOverTimeForBomb = 0.3f;
    public int CollectedCoinCount = 0;
    int lastBombIndex = -1;
    public GameObject TitleUI;
    public GameObject GameUI;
    public float AutoStepTime = 0.1f;
    public float NormalStepTime = 0.1f;
    public MultiPlayState MultiplayState;
    public int MultiplayGoalScore = 50;
    public Image StartSign;
    bool _isReadyToGo = false;
    bool _isMultiplayGame = false;
    // Use this for initialization
    void Start()
    {
        TitleUI.SetActive(true);
        GameUI.SetActive(false);
        GameOverMenu.SetActive(false);
        
        int continuousDayCount = GameManager.Instance.GetContinuousDayCount();
        AchievementManager.Instance.UpdateCurrentProgress(AchievementType.FiveDaysInARow, continuousDayCount);

    }
    private void Awake()
    {
        GameManager.Instance.World = this;
        GameManager.Instance.PlayBGM(gameObject);
        UpdateCharacter();
    }
    void CreateTiles()
    {
        int rowCount = (int)(Size.y / YGap);
        tileCountShouldBe = 0;
        for (int y = -2; y < rowCount + 2; y++)
        {
            for (int i = 0; i < 3; i++)
            {
                GameObject obj = Instantiate(NormalTilePrefab);
                _tileList.Add(obj.GetComponent<Tile>());
                obj.transform.position = new Vector3((i - 1) * XGap, OffsetY +  y * YGap);
                tileCountShouldBe++;
                obj.transform.SetParent(transform);
                obj.SetActive(true);
            }
        }
    }
    public void OnAutoClick()
    {
        StartCoroutine(Stepping());
    }
    IEnumerator Stepping()
    {
        SetStepTime(AutoStepTime);
        List<Tile> fourthLine = _tileList.GetRange(4 * 3, 3);
        List<Tile> fifthLine = _tileList.GetRange(4 * 3, 3);
        List<Leg> legList = new List<Leg>();
        legList.Add(LeftLeg);
        legList.Add(MiddleLeg);
        legList.Add(RightLeg);
        foreach (Leg leg in legList)
        {
            leg.IsSuper = true;
        }
        int startScore = Score;
        while (true)
        {
            fourthLine = _tileList.GetRange(3 * 3, 3);
            fifthLine = _tileList.GetRange(4 * 3, 3);
            Tile bombInFourth = null;
            int index = -1;
            for (int i = 0; i < 3; i++)
            {
                if (fourthLine[i].TileType == TileType.TileBomb)
                {
                    bombInFourth = fourthLine[i];
                    index = i;
                    break;
                }
            }
            if (bombInFourth != null)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (index != i && legList[i].LegPosition == LegPosition.Near)
                    {
                        ClickCorespondingLegButton(legList[i]);
                        yield return new WaitForSeconds(StepTime);
                        break;
                    }
                }
                ClickCorespondingLegButton(legList[index]);
                yield return new WaitForSeconds(StepTime);
            }
            else
            {
                Tile bombInFifth = null;
                for (int i = 0; i < 3; i++)
                {
                    if (fifthLine[i].TileType == TileType.TileBomb)
                    {
                        bombInFifth = fifthLine[i];
                        index = i;
                        break;
                    }
                }
                if (bombInFifth != null)
                {
                    if (legList[index].LegPosition == LegPosition.Near)
                    {
                        ClickCorespondingLegButton(legList[index]);
                        yield return new WaitForSeconds(StepTime);
                    }
                    for (int i = 0; i < 3; i++)
                    {
                        if (index != i && legList[i].LegPosition == LegPosition.Near)
                        {
                            ClickCorespondingLegButton(legList[i]);
                            yield return new WaitForSeconds(StepTime);
                            break;
                        }
                    }
                }else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (legList[i].LegPosition == LegPosition.Near)
                        {
                            ClickCorespondingLegButton(legList[i]);
                            yield return new WaitForSeconds(StepTime);
                            break;
                        }
                    }
                }
            }
            if (startScore + 30 <= Score || IsGameOver)
            {
                break;
            }
        }
        foreach (Leg leg in legList)
        {
            leg.IsSuper = false;
        }
        SetStepTime(NormalStepTime);
        legList.Clear();
    }
    void SetStepTime(float time)
    {
        StepTime = time;
        LeftLeg.StepTime = time;
        MiddleLeg.StepTime = time;
        RightLeg.StepTime = time;

    }
    void ClickCorespondingLegButton(Leg leg)
    {
        if (leg == LeftLeg)
        {
            OnLeftClick();
        }
        else if (leg == MiddleLeg)
        {
            OnMiddleClick();
        }
        else if (leg == RightLeg)
        {
            OnRightClick();
        }
    }
    void MoveTile()
    {
        Invoke("ScorePoint", 0.1f);
        // move tiles
        int rowCount = (int)(Size.y / YGap);
        int tileIndex = 0;
        for (int y = -2; y < rowCount + 2; y++)
        {
            for (int i = 0; i < 3; i++)
            {
                Tile tile = _tileList[tileIndex];
                tile.gameObject.transform.DOMove(new Vector3((i - 1) * XGap, OffsetY + (y-1) * YGap), StepTime);
                tileIndex++;
            }
        }
        // remove tiles
        for (int i = 0; i < 3; i++)
        {
            Tile tileToRemove = _tileList[0];
            _tileList.RemoveAt(0);
            Destroy(tileToRemove.gameObject);
        }
        // create bomb
        int bombIndex = -1;
        if (Random.Range(0, 100) <= 50)
        {
            while (true)
            {
                bombIndex = Random.Range(0, 3);
                if (lastBombIndex != bombIndex)
                {
                    break;
                }
            }
        }
        // create tiles
        lastBombIndex = bombIndex;
        int coinIndex = Random.Range(0, 2);
        for (int i = 0; i < 3; i++)
        {
            GameObject obj;
            if (bombIndex == i)
            {
                obj = Instantiate(BombTile);
            }
            else if(bombIndex < 0 && coinIndex == i && Random.Range(0, 100) <= 100)
            {
                obj = Instantiate(CoinTile);
            }else
            {
                obj = Instantiate(NormalTilePrefab);
            }
            obj.transform.position = new Vector3((i - 1) * XGap, _tileList[_tileList.Count - 1].transform.position.y);
            Tile tile = obj.GetComponent<Tile>();
            obj.transform.SetParent(transform);
            obj.SetActive(true);
            obj.name = "new tile";
            _tileList.Add(tile);
        }
    }
    // Update is called once per frame
    void Update() {
        if (IsStepedOnNormal)
        {
            //Invoke("ScorePoint", 0.1f);
        }else if (IsStepedOnBomb)
        {
            IsStepedOnBomb = false;
            //Invoke("GameOver", 0.7f);
        }
    }
    public void StartMultiplayGame()
    {
        StartSign.transform.DOMoveX(-1400, 0.001f);
        StartSign.transform.DOMoveX(400, 0.5f).SetDelay(0.001f).SetEase(Ease.OutElastic);
        MultiplayState.Init();
        Restart();
        _isReadyToGo = false;
        _isMultiplayGame = true;
        Invoke("SetRed", 1);
        Invoke("SetYellow", 2);
        Invoke("SetGreen", 3);
    }
    IEnumerator SendingMyScore()
    {
        while (!IsGameOver)
        {

            yield return new WaitForSeconds(2);
        }
    }
    public void SetRed()
    {
        StartSign.sprite = Resources.Load<Sprite>("ui/multiplay/signRed");
        GameManager.Instance.PlaySound(StartSign.gameObject);
    }
    public void SetYellow()
    {
        StartSign.sprite = Resources.Load<Sprite>("ui/multiplay/signYellow");
        GameManager.Instance.PlaySound(StartSign.gameObject);
    }
    public void SetGreen()
    {
        StartSign.sprite = Resources.Load<Sprite>("ui/multiplay/signGreen");
        GameManager.Instance.PlaySound(StartSign.gameObject);
        StartSign.transform.DOMoveX(1400, 0.5f).SetDelay(0.5f).SetEase(Ease.InElastic);
        GameUI.GetComponent<GameUI>().ShowGo();
        _isReadyToGo = true;
    }
    public void SetPlayerState(MultiPlayerInfo player)
    {

    }
    public void GivePanelty()
    {

    }
    public void AddCoin()
    {
        CollectedCoinCount++;
        AchievementManager.Instance.UnlockAchievement(AchievementType.Collect1Coin);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Collect10CoinsTotal);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Collect50CoinsTotal);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Collect100CoinsTotal);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Collect200CoinsTotal);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Collect500CoinsTotal);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Collect1000CoinsTotal);

        AchievementManager.Instance.UpdateCurrentProgress(AchievementType.Collect5CoinsInSinglePlay, CollectedCoinCount);
        AchievementManager.Instance.UpdateCurrentProgress(AchievementType.Collect10CoinsInSinglePlay, CollectedCoinCount);
        AchievementManager.Instance.UpdateCurrentProgress(AchievementType.Collect15CoinsInSinglePlay, CollectedCoinCount);
    }
    public void ShowPause()
    {
        PauseMenu.SetActive(true);
    }
    public void OnResumeClick()
    {
        PauseMenu.SetActive(false);
    }
    public void OnBackClick()
    {
        RemoveTiles();
        TitleUI.SetActive(true);
        GameUI.SetActive(false);
    }
    public void GameOver()
    {
        if (IsGameOver)
        {
            return;
        }
        IsGameOver = true;
        Debug.Log("Game Over");
        AchievementManager.Instance.SaveAchievementData();
        if (ContinueUsed)
        {
            GameOverMenu.SetActive(true);
            GameOverMenu.GetComponent<GameOverPopup>().StartCounting(CollectedCoinCount, Score);
        }else
        {
            ContinuePopup.gameObject.SetActive(true);
            ContinuePopup.StartCounting();
        }
    }
    public void OnCloseContinue()
    {
        AdmobManager.Instance.ShowInterstitial();
        ContinuePopup.gameObject.SetActive(false);
        GameOverMenu.SetActive(true);
        GameOverMenu.GetComponent<GameOverPopup>().StartCounting(CollectedCoinCount, Score);
    }
    public void Log(string text)
    {
        LblDebug.text = text;
    }
    public void OnContinued()
    {
        AchievementManager.Instance.UnlockAchievement(AchievementType.Continue);
        ContinueUsed = true;
        AdmobManager.Instance.Init();
        GameUI.SetActive(true);
        PauseMenu.SetActive(false);
        GameOverMenu.SetActive(false);
        ContinuePopup.OnContinued();
        IsGameOver = false;
        UpdateCharacter();
    }
    public void OnShareClick()
    {
        AchievementManager.Instance.UnlockAchievement(AchievementType.Share);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Share3Times);
    }
    public void On30MeterClick()
    {

    }
    public void StartFrom30Meter()
    {
        AchievementManager.Instance.UnlockAchievement(AchievementType.StartFrom30);
    }

    public void OnRestartClick()
    {
        AchievementManager.Instance.UnlockAchievement(AchievementType.TryAgain);
        Restart();
    }
    public void Restart()
    {
        _isMultiplayGame = false;
        _isReadyToGo = true;
        GameUI.GetComponent<GameUI>().EnableGameStartPopup();
        IsGameOver = false;
        ContinueUsed = false;
        AdmobManager.Instance.Init();
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Play3Times);
        GameUI.SetActive(true);
        PauseMenu.SetActive(false);
        GameOverMenu.SetActive(false);

        CollectedCoinCount = 0;
        Score = 0;
        LblScore.text = "0";
        UpdateCharacter();
        RemoveTiles();
        CreateTiles();
        
    }
    public void UpdateCharacter()
    {
        LeftLeg.GoToStart();
        RightLeg.GoToStart();
        MiddleLeg.GoToStart();
        transform.Find("body").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(string.Format("images/body_{0}", GameManager.Instance.GameData.SelectedCharacter));
    }
    void RemoveTiles()
    {
        foreach (Tile tile in _tileList)
        {
            Destroy(tile.gameObject);
        }
        _tileList.Clear();
    }
    void ScorePoint()
    {
        Score++;
        LblScore.text = string.Format("{0}m", Score.ToString());
        AddScoreAchievement(Score);
    }
    public void GameOverWithBomb()
    {
        if (_isMultiplayGame)
        {
            GivePanelty();
        }else
        {
            Invoke("GameOver", gameOverTimeForBomb);
        }
    }
    void AddStepAchievement()
    {
        AchievementManager.Instance.UnlockAchievement(AchievementType.TheFirstStep);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Total100Steps);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Total1000Steps);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Total10000Steps);
    }
    void AddScoreAchievement(int score)
    {
        AchievementManager.Instance.UnlockAchievement(AchievementType.GoGoGo);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Total100Meters);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Total1000Meters);
        AchievementManager.Instance.AddCurrentProgress(AchievementType.Total10000Meters);
        AchievementManager.Instance.UpdateCurrentProgress(AchievementType.TenMeters, score);
        AchievementManager.Instance.UpdateCurrentProgress(AchievementType.FiftyMeters, score);
        AchievementManager.Instance.UpdateCurrentProgress(AchievementType.HundredMeters, score);
    }
    public void OnMiddleClick()
    {
        if (IsGameOver)
        {
            return;
        }
        if (!_isReadyToGo)
        {
            GivePanelty();
            return;
        }
        AddStepAchievement();
        // check torn
        if (Mathf.Abs((int)MiddleLeg.LegPosition + 1 - (int)LeftLeg.LegPosition) > 1 ||
            Mathf.Abs((int)MiddleLeg.LegPosition + 1 - (int)RightLeg.LegPosition) > 1)
        {
            if (_isMultiplayGame)
            {
                GivePanelty();
            }
            else
            {
                MiddleLeg.StepForward();
                MiddleLeg.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(string.Format("images/legTorn_{0}", GameManager.Instance.GameData.SelectedCharacter));
                Invoke("GameOver", gameOverTimeForTorn);
            }
            return;
        }
        
        // big step
        if (MiddleLeg.LegPosition == LegPosition.Near &&
            LeftLeg.LegPosition != LegPosition.Near &&
            RightLeg.LegPosition != LegPosition.Near)
        {
            MiddleLeg.SkipOne = true;
            MiddleLeg.StepForward();
            LeftLeg.StepBack();
            RightLeg.StepBack();
            MoveTile();
        }
        else
        {
            MiddleLeg.StepForward();
        }
    }
    public void OnLeftClick()
    {
        if (IsGameOver)
        {
            return;
        }
        if (!_isReadyToGo)
        {
            GivePanelty();
            return;
        }
        AddStepAchievement();
        // check torn
        if (Mathf.Abs((int)LeftLeg.LegPosition + 1 - (int)MiddleLeg.LegPosition) > 1)
        {
            if (_isMultiplayGame)
            {
                GivePanelty();

            }
            else
            {
                LeftLeg.StepForward();
                LeftLeg.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(string.Format("images/legTorn_{0}", GameManager.Instance.GameData.SelectedCharacter));
                Invoke("GameOver", gameOverTimeForTorn);
            }
            return;
        }

        // big step
        if (MiddleLeg.LegPosition != LegPosition.Near &&
            LeftLeg.LegPosition == LegPosition.Near &&
            RightLeg.LegPosition != LegPosition.Near)
        {
            LeftLeg.SkipOne = true;
            LeftLeg.StepForward();
            MiddleLeg.StepBack();
            RightLeg.StepBack();
            MoveTile();
        }
        else
        {
            LeftLeg.StepForward();
        }
    }
    public void OnRightClick()
    {
        if (IsGameOver)
        {
            return;
        }
        if (!_isReadyToGo)
        {
            GivePanelty();
            return;
        }
        AddStepAchievement();
        // check torn
        int right = (int)RightLeg.LegPosition + 1;
        int middle = (int)MiddleLeg.LegPosition;
        int abs = Mathf.Abs(right - middle);
        if (Mathf.Abs((int)RightLeg.LegPosition + 1 - (int)MiddleLeg.LegPosition) > 1)
        {
            if (_isMultiplayGame)
            {
                GivePanelty();
            }
            else
            {
                RightLeg.StepForward();
                RightLeg.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(string.Format("images/legTorn_{0}", GameManager.Instance.GameData.SelectedCharacter));
                Invoke("GameOver", gameOverTimeForTorn);
            }
            return;
        }
 
        // big step
        if (MiddleLeg.LegPosition != LegPosition.Near &&
            LeftLeg.LegPosition != LegPosition.Near &&
            RightLeg.LegPosition == LegPosition.Near)
        {
            RightLeg.SkipOne = true;
            RightLeg.StepForward();
            MiddleLeg.StepBack();
            LeftLeg.StepBack();
            MoveTile();
        }
        else
        {
            RightLeg.StepForward();
        }
    }
}
