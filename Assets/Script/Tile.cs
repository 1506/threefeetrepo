﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    TileNormal,
    TileBomb,
    TileCoin

}
public class Tile : MonoBehaviour {
    public TileType TileType;
    public static float TileGapY = 1.5f;
    public GameObject bombPrefab;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Caboom()
    {
        GameObject obj = Instantiate(bombPrefab);
        obj.transform.position = transform.position;
    }
}
