﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupStarter : MonoBehaviour {
    public GameObject PopupObject;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowPopup()
    {
        GameObject obj = Instantiate(PopupObject) as GameObject;
        obj.transform.SetParent(transform.parent, false);
        GameManager.Instance.CurrentPopup = obj;
    }
}
