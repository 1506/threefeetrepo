﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum EffectType
{
    ScaleEndFade
}
public class Effect : MonoBehaviour {
    public EffectType EffectType = EffectType.ScaleEndFade;
	// Use this for initialization
	void Start () {
        if (EffectType == EffectType.ScaleEndFade)
        {
            GameManager.Instance.ScaleAndFadeOut(gameObject, 0.3f, 1, 0.3f, 0.2f);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
