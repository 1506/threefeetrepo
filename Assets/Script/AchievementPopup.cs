﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AchievementPopup : MonoBehaviour {
    public GameObject ItemPrefab;
    public GameObject BtnAchievement;
    private List<Vector2> _positionList = new List<Vector2>();
    List<AchievementItem> AchievedNotRewaredList = new List<AchievementItem>();
    List<AchievementItem> NotAchievedList = new List<AchievementItem>();
    List<AchievementItem> AchievedRewaredList = new List<AchievementItem>();
    public List<AchievementItem> AchievementItemList = new List<AchievementItem>();
    Transform Container;
    float _gapY = 170;
    // Use this for initialization
    void Start () {
        AchievementItemList.Clear();
        _positionList.Clear();
        Container = transform.Find("svAchievement").Find("Viewport").Find("Content");
        int index = 0;
        int totalCount = AchievementManager.Instance.GetTotalAchievementCount();
        for ( int i = 0; i < totalCount;i++)
        {
            GameObject child = Instantiate(ItemPrefab);
            AchievementItem item = child.GetComponent<AchievementItem>();
            item.MaxProgress = AchievementManager.Instance.GetMaxProgress((AchievementType)i);
            item.CurrentProgress = AchievementManager.Instance.GetCurrentProgress((AchievementType)index);
            item.iOSKey = AchievementManager.Instance.TheData.iOSKeyList[i];
            item.AndroidKey = AchievementManager.Instance.TheData.androidKeyList[i];
            child.transform.Find("lblTitle").GetComponent<Localizer>().LocalizingKey = string.Format("achievement title {0}", i);
            child.transform.Find("lblDescription").GetComponent<Localizer>().LocalizingKey = string.Format("achievement description {0}", i);
            child.transform.SetParent(Container);
            AchievementItemList.Add(item);
            _positionList.Add(item.transform.localPosition);
            index++;
        }
        RectTransform rect = transform.Find("svAchievement").Find("Viewport").Find("Content").GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, _gapY * totalCount + 30);
        transform.Find("svAchievement").Find("Viewport").GetComponent<Mask>().enabled = true; 
        gameObject.SetActive(false);
        UpdateItems();
    }
    
    public void ShowAchievement()
    {
        // test now
        for (int i = 0; i < 34; i++)
        {
            AchievementManager.Instance.TheData.RewardedList[i] = false;
        }
        BtnAchievement.GetComponent<AlarmShaker>().StopAlarm();
        gameObject.SetActive(true);
    }
    public void CheckAchievement()
    {
        if (true)
        {
            BtnAchievement.GetComponent<AlarmShaker>().StartAlarm();
        }
    }
    // Update is called once per frame
    void Update () {

    }
    void UpdateProgress()
    {
        int index = 0;
        foreach (AchievementItem item in AchievementItemList)
        {
            item.SetProgress(AchievementManager.Instance.GetCurrentProgress((AchievementType)index));
            index++;
        }
    }
    void UpdatePositions()
    {
        int count = 0;
        NotAchievedList.Clear();
        AchievedRewaredList.Clear();
        AchievedNotRewaredList.Clear();
        foreach (AchievementItem item in AchievementItemList)
        {
            //item.transform.SetParent(transform);
            count++;
            if (AchievementManager.Instance.TheData.MaxProgressList[count - 1] > AchievementManager.Instance.TheData.CurrentProgressList[count - 1])
            {
                NotAchievedList.Add(item);
            }
            else
            {
                if (AchievementManager.Instance.TheData.RewardedList[count - 1])
                {
                    AchievedRewaredList.Add(item);
                }
                else
                {
                    AchievedNotRewaredList.Add(item);
                }
            }
        }
        int index = 0;
        float offsetY = 956;
        float moveTime = 0.4f;
        float x = 387;
        foreach (AchievementItem item in AchievedNotRewaredList)
        {
            // move position
            //item.transform.SetParent(Container);
            //item.transform.localPosition = _positionList[index];
            item.transform.DOMove(new Vector3(x, offsetY - index*_gapY), moveTime);
            item.transform.Find("btnReward").GetComponent<Image>().color = new Color(1, 1, 1, 1);
            item.transform.Find("btnReward").Find("imgTrophy").gameObject.SetActive(false);
            item.transform.Find("btnReward").Find("imgCoin").gameObject.SetActive(true);
            item.transform.Find("btnReward").Find("lblCoin").gameObject.SetActive(true);
            item.transform.Find("lblDescription").Find("imgChecked").gameObject.SetActive(false);
            index++;
        }
        foreach (AchievementItem item in NotAchievedList)
        {
            // move position
            //item.transform.SetParent(Container);
            //item.transform.localPosition = _positionList[index];
            item.transform.DOMove(new Vector3(x, offsetY - index * _gapY), moveTime);
            item.transform.Find("btnReward").GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 1);
            item.transform.Find("btnReward").Find("imgTrophy").gameObject.SetActive(false);
            item.transform.Find("btnReward").Find("imgCoin").gameObject.SetActive(true);
            item.transform.Find("btnReward").Find("lblCoin").gameObject.SetActive(true);
            item.transform.Find("lblDescription").Find("imgChecked").gameObject.SetActive(false);
            index++;
        }
        foreach (AchievementItem item in AchievedRewaredList)
        {
            // move position
            //item.transform.SetParent(Container);
            //item.transform.localPosition = _positionList[index];
            item.transform.DOMove(new Vector3(x, offsetY - index * _gapY), moveTime);
            item.transform.Find("btnReward").GetComponent<Image>().color = new Color(0, 0, 0, 1);
            item.transform.Find("btnReward").Find("imgTrophy").gameObject.SetActive(true);
            item.transform.Find("btnReward").Find("imgCoin").gameObject.SetActive(false);
            item.transform.Find("btnReward").Find("lblCoin").gameObject.SetActive(false);
            item.transform.Find("lblDescription").Find("imgChecked").gameObject.SetActive(true);
            index++;
        }
    }
    public void UpdateItems()
    {
        UpdateProgress();
        UpdatePositions();
    }

    public void OnCloseClick()
    {
        AchievementManager.Instance.SaveAchievementData();
        gameObject.SetActive(false);
    }
}
