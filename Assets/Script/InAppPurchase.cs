﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;

public enum ProductIDs
{
    RemoveAds,
    ProductB,
    ProductC,
    ProductD,
    ProductE,

}
public class InAppPurchase : MonoBehaviour, IStoreListener
{
    private static InAppPurchase _instance;
    public static InAppPurchase Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(InAppPurchase)) as InAppPurchase;
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    _instance = container.AddComponent(typeof(InAppPurchase)) as InAppPurchase;
                }
            }

            return _instance;
        }
    }
    private static IStoreController _storeController;
    private static IExtensionProvider _extensionProvider;
    public event EventHandler Restore;
    
    public event OnEventDelegate<ProductIDs> Purchased;
    public string GetProduct(ProductIDs id)
    {
        switch (id)
        {
            case ProductIDs.RemoveAds:
                return "com.magmon.steppymonster.removeads";
            case ProductIDs.ProductB:
                return "productA";
            case ProductIDs.ProductC:
                return "productA";
            case ProductIDs.ProductD:
                return "productA";
            case ProductIDs.ProductE:
                return "productA";
            default:
                break;
        }
        return null;
    }

    void Start()
    {
        InitializePurchasing();
    }

    private bool IsInitialized()
    {
        return (_storeController != null && _extensionProvider != null);
    }

    public void InitializePurchasing()
    {
        if (IsInitialized())
            return;

        var module = StandardPurchasingModule.Instance();

        ConfigurationBuilder builder = ConfigurationBuilder.Instance(module);




        var values = Enum.GetValues(typeof(ProductIDs));
        string strID;
        foreach (ProductIDs productID in Enum.GetValues(typeof(ProductIDs)))
        {
            strID = GetProduct(productID);
            builder.AddProduct(strID, ProductType.Consumable, new IDs
            {
            {strID,AppleAppStore.Name},
            {strID,GooglePlay.Name},
            });
        }

        UnityPurchasing.Initialize(this, builder);
    }
    public string GetPrice(ProductIDs productID)
    {
        return _storeController.products.WithID(GetProduct(productID)).metadata.localizedPriceString;
    }
    public void BuyProductID(ProductIDs id)
    {
        string productId = GetProduct(id);
        try
        {
            if (IsInitialized())
            {
                Product p = _storeController.products.WithID(productId);

                if (p != null && p.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", p.definition.id));
                    _storeController.InitiatePurchase(p);
                }
                else
                {
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }
        catch (Exception e)
        {
            Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);
        }
    }

    public void RestorePurchase()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = _extensionProvider.GetExtension<IAppleExtensions>();

            apple.RestoreTransactions
            (
            (result) => 
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            }
            );
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController sc, IExtensionProvider ep)
    {
        Debug.Log("OnInitialized : PASS");

        _storeController = sc;
        _extensionProvider = ep;
    }

    public void OnInitializeFailed(InitializationFailureReason reason)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + reason);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        
        if (Purchased != null)
        {
            string id = args.purchasedProduct.definition.id;
            if (id.Equals(GetProduct(ProductIDs.RemoveAds)))
            {
                Purchased(ProductIDs.RemoveAds);
            }
        }

        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        string log = string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason);
        Debug.Log(log);
    }
}